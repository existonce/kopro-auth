brew install llvm libsodium rocksdb
export LDFLAGS="-L/opt/homebrew/opt/openssl@1.1/lib"
export CPPFLAGS="-I/opt/homebrew/opt/openssl@1.1/include"
export LDFLAGS="-L/opt/homebrew/opt/llvm/lib $LDFLAGS"
export CPPFLAGS="-I/opt/homebrew/opt/llvm/include $CPPFLAGS"
export PATH="/opt/homebrew/opt/llvm/bin:$PATH"
export SODIUM_LIB_DIR="/opt/homebrew/opt/libsodium/lib"
export SODIUM_STATIC="YES"
export ROCKSDB_LIB_DIR="/opt/homebrew/opt/rocksdb/lib"

