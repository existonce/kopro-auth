extern crate chrono;
extern crate clap;
extern crate thiserror;
extern crate tokio;
extern crate yggdrasil_client;

pub mod handler;
pub mod website;

use website::*;

use clap::Parser;
use std::error::Error;
use std::net::{IpAddr, SocketAddr};

#[derive(Parser, Debug)]
#[clap(version, author = "schoolgirl <existonce@protonmail.com>")]
struct Opts {
    #[clap(short, long, default_value = "0.0.0.0")]
    host: IpAddr,
    #[clap(short, long, default_value = "3002")]
    port: u16,
    #[clap(short, long, default_value = "./static")]
    statics: String,
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    let opts: Opts = Opts::parse();

    let addr = SocketAddr::new(opts.host, opts.port);
    println!("Start serving at {}", addr);
    println!("Static files are at {}", opts.statics);
    serve_website(addr, opts.statics).await;
    Ok(())
}
