use crate::handler::guide::*;
use crate::handler::index::*;
use std::net::SocketAddr;
use warp::Filter;

pub async fn serve_website(addr: SocketAddr, static_path: String) {
    let index_route = warp::path::end().and_then(index_handler);
    let guide_route = warp::path("guide").and_then(guide_handler);
    let static_route = warp::path("static").and(warp::fs::dir(static_path));
    let routes = index_route.or(guide_route).or(static_route);

    warp::serve(routes).run(addr).await;
}
