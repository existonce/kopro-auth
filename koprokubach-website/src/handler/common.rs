use askama::Template;
use thiserror::Error;
use warp::Rejection;

pub type WebResult<T> = std::result::Result<T, Rejection>;

#[derive(Error, Debug)]
pub enum Error {
    #[error("templating error: {0}")]
    TemplateError(#[from] askama::Error),
}

#[derive(Template)]
#[template(path = "error.html")]
pub struct ErrorTemplate {
    message: &'static str,
}

impl warp::reject::Reject for Error {}
