use super::common::*;
use askama::Template;
use warp::{reject, reply::html, Reply};

#[derive(Template)]
#[template(path = "index.html")]
struct IndexTemplate<'a> {
    thread: &'a str,
}

pub async fn index_handler() -> WebResult<impl Reply> {
    let template = IndexTemplate {
        thread: "https://2ch.hk/mc/res/564598.html",
    };
    let res = template
        .render()
        .map_err(|e| reject::custom(Error::TemplateError(e)))?;
    Ok(html(res))
}
