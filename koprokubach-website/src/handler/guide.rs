use super::common::*;
use askama::Template;
use warp::{reject, reply::html, Reply};

#[derive(Template)]
#[template(path = "guide.html")]
struct GuideTemplate;

pub async fn guide_handler() -> WebResult<impl Reply> {
    let res = GuideTemplate
        .render()
        .map_err(|e| reject::custom(Error::TemplateError(e)))?;
    Ok(html(res))
}
