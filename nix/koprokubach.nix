{config, lib, ...}:
{
  imports = [
    ./website.nix
    ./yggdrasil.nix
  ];
  config = {
    nixpkgs.overlays = [
      (import ./overlay.nix)
    ];
    services.kopro-yggdrasil = {
      enable = true;
      port = 3002;
    };
    services.koprokubach-website = {
      enable = true;
      port = 3001;
    };

  };
}
