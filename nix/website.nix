{ config, lib, pkgs, ... }:
with lib;
let cfg = config.services.koprokubach-website;
in {
  options = {
    services.koprokubach-website = {
      enable = mkOption {
        type = types.bool;
        default = false;
        description = ''
          Whether to enable koprokubach-website by default.
        '';
      };
      static = mkOption {
        type = types.path;
        default = ../koprokubach-website/static;
        description = ''
          Path to static files to serve with web server.
        '';
      };
      host = mkOption {
        type = types.str;
        default = "0.0.0.0";
        description = ''
          Which host to listen for incoming connections.
        '';
      };
      port = mkOption {
        type = types.int;
        default = 3002;
        description = ''
          Which port to listen for incoming connections.
        '';
      };
    };
  };

  config = mkIf cfg.enable {
    systemd.services.koprokubach-website = {
      enable = true;
      description = "Koprokubach website service kubach.day";
      after = ["network.target"];
      wants = ["network.target"];
      script = ''
        ${pkgs.koprokubach}/bin/koprokubach-website --host ${cfg.host} \
          --port ${builtins.toString cfg.port} \
          --statics ${cfg.static}
      '';
      serviceConfig = {
          Restart = "always";
          RestartSec = 30;
          User = "koprokubach-website";
          LimitNOFILE = 10000;
      };
      wantedBy = ["multi-user.target"];
    };
    users.users.koprokubach-website = {
      description   = "Kubach.day website daemon user";
      isSystemUser  = true;
      group         = "koprokubach-website";
    };
    users.groups.koprokubach-website = {};
  };

}
