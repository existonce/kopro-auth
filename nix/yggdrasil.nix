{ config, lib, pkgs, ... }:
with lib;
let cfg = config.services.kopro-yggdrasil;
in {
  options = {
    services.kopro-yggdrasil = {
      enable = mkOption {
        type = types.bool;
        default = false;
        description = ''
          Whether to enable yggdrasil by default.
        '';
      };
      data = mkOption {
        type = types.path;
        default = "/var/lib/kopro-yggdrasil";
        description = ''
          Path to static files to serve with web server.
        '';
      };
      host = mkOption {
        type = types.str;
        default = "0.0.0.0";
        description = ''
          Which host to listen for incoming connections.
        '';
      };
      port = mkOption {
        type = types.int;
        default = 3003;
        description = ''
          Which port to listen for incoming connections.
        '';
      };
    };
  };

  config = mkIf cfg.enable {
    systemd.services.kopro-yggdrasil = {
      enable = true;
      description = "Koprokubach auth service";
      after = ["network.target"];
      wants = ["network.target"];
      script = ''
        ${pkgs.koprokubach}/bin/yggdrasil-server --host ${cfg.host} \
          --port ${builtins.toString cfg.port} \
          --homepage-url https://kubach.day \
          --register-url https://kubach.day/register \
          --server-name Koprokubach \
          --data ${cfg.data}
      '';
      serviceConfig = {
          Restart = "always";
          RestartSec = 30;
          User = "kopro-yggdrasil";
          LimitNOFILE = 10000;
      };
      wantedBy = ["multi-user.target"];
    };
    users.users.kopro-yggdrasil = {
      description   = "Kubach.day website daemon user";
      isSystemUser  = true;
      group         = "kopro-yggdrasil";
    };
    users.groups.kopro-yggdrasil = {};

    system.activationScripts = {
      init-kopro-yggdrasil = {
        text = ''
          if [ ! -d "${cfg.data}" ]; then
            mkdir -p ${cfg.data}
            chown -R kopro-yggdrasil ${cfg.data}
          fi
        '';
        deps = [];
      };
    };
  };

}
