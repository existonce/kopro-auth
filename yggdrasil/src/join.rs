use serde::{Deserialize, Serialize};
use std::net::IpAddr;

pub use super::profile::{ProfileId, UserProfile, UserProperty};
pub use super::token::{AccessToken, ServerId};

/// Both server and client need to make a request to sessionserver.mojang.com if the server is in online-mode.
///
/// Endpoint `sessionserver/session/minecraft/join`
///
/// **Note**: The fields <accessToken> and the player's uuid were received by the client during authentication.
///
/// If everything goes well, the client will receive a "HTTP/1.1 204 No Content" response.
#[derive(Deserialize, Serialize, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[serde(rename_all = "camelCase")]
pub struct JoinPayload {
    /// hexadecimal or JSON-Web-Token (unconfirmed) [The normal accessToken can be found in the
    /// payload of the JWT (second by '.' separated part as Base64 encoded JSON object), in key
    /// "yggt"]
    pub access_token: AccessToken,
    /// For most recent version is empty string
    pub server_id: ServerId,
    /// UUID without dashes of user profile
    pub selected_profile: ProfileId,
}

/// After decrypting the shared secret in the second Encryption Response, the server generates the login hash as above and sends a HTTP GET to
///
/// Endpoint `sessionserver/session/minecraft/hasJoined?username=username&serverId=hash&ip=ip`
///
/// The username is case insensitive and must match the client's username (which was received in the Login Start packet). Note that this is the in-game nickname of the selected profile, not the Mojang account name (which is never sent to the server). Servers should use the name sent in the "name" field.
///
/// The ip field is optional and when present should be the IP address of the connecting player; it is the one that originally initiated the session request. The notchian server includes this only when prevent-proxy-connections is set to true in server.properties.
///
/// The response is a JSON object containing the user's UUID and skin blob
#[derive(Deserialize, Serialize, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[serde(rename_all = "camelCase")]
pub struct HasJoinedPayload {
    pub username: String,
    pub server_id: String,
    pub ip: Option<IpAddr>,
}

/// After decrypting the shared secret in the second Encryption Response, the server generates the login hash as above and sends a HTTP GET to
///
/// Endpoint `sessionserver/session/minecraft/hasJoined?username=username&serverId=hash&ip=ip`
///
/// The username is case insensitive and must match the client's username (which was received in the Login Start packet). Note that this is the in-game nickname of the selected profile, not the Mojang account name (which is never sent to the server). Servers should use the name sent in the "name" field.
///
/// The ip field is optional and when present should be the IP address of the connecting player; it is the one that originally initiated the session request. The notchian server includes this only when prevent-proxy-connections is set to true in server.properties.
///
/// The response is a JSON object containing the user's UUID and skin blob
#[derive(Deserialize, Serialize, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[serde(rename_all = "camelCase")]
pub struct HasJoined {
    pub id: ProfileId,
    pub name: String,
    pub properties: Option<Vec<UserProperty>>,
}
