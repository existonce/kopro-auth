use serde::{Deserialize, Serialize};

/// Invalidates accessTokens using an account's username and password.
///
/// Endpoint `authserver/signout`
///
#[derive(Deserialize, Serialize, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[serde(rename_all = "camelCase")]
pub struct SignoutPayload {
    pub username: String,
    pub password: String,
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_signout_payload() {
        let data = r#"
        {
            "username": "mojang account name",
            "password": "mojang account password"
        }"#;

        let v: SignoutPayload = serde_json::from_str(data).unwrap();

        assert_eq!(
            v,
            SignoutPayload {
                username: "mojang account name".to_string(),
                password: "mojang account password".to_string(),
            }
        )
    }
}
