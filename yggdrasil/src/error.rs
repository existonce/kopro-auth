use serde::{Deserialize, Serialize};

/// If however a request fails, the server will respond with:
/// * An appropriate, non-200 HTTP status code
/// * A JSON-encoded dictionary following this format:
#[derive(Deserialize, Serialize, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[serde(rename_all = "camelCase")]
pub struct Error {
    pub error: String,
    pub error_message: String,
    pub cause: Option<String>,
}

impl std::error::Error for Error {}

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match &self.cause {
            Some(cause) => write!(
                f,
                "{}: {} caused by {}",
                self.error, self.error_message, cause
            ),
            None => write!(f, "{}: {}", self.error, self.error_message),
        }
    }
}

impl Error {
    pub fn invalid_credentials() -> Self {
        Error {
            error: "ForbiddenOperationException".to_owned(),
            error_message: "Invalid credentials. Invalid username or password.".to_owned(),
            cause: None,
        }
    }

    pub fn missing_user() -> Self {
        Error {
            error: "UserNotExists".to_owned(),
            error_message: "Invalid credentials. User is not registered yet.".to_owned(),
            cause: None,
        }
    }

    pub fn user_registered() -> Self {
        Error {
            error: "ForbiddenOperationException".to_owned(),
            error_message: "Given user is already registered".to_owned(),
            cause: None,
        }
    }

    pub fn rate_limited() -> Self {
        Error {
            error: "ForbiddenOperationException".to_owned(),
            error_message: "Too many requests".to_owned(),
            cause: None,
        }
    }

    pub fn database_error(error: &str) -> Self {
        Error {
            error: "InternalDatabaseError".to_owned(),
            error_message: error.to_owned(),
            cause: None,
        }
    }

    pub fn token_not_valid() -> Self {
        Error {
            error: "ForbiddenOperationException".to_owned(),
            error_message: "Token not valid".to_owned(),
            cause: None,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn error_test1() {
        let data = r#"
        {
            "error": "Short description of the error",
            "errorMessage": "Longer description which can be shown to the user",
            "cause": "Cause of the error"
        }"#;

        let v: Error = serde_json::from_str(data).unwrap();

        assert_eq!(
            v,
            Error {
                error: "Short description of the error".to_string(),
                error_message: "Longer description which can be shown to the user".to_string(),
                cause: Some("Cause of the error".to_string()),
            }
        )
    }

    #[test]
    fn error_test2() {
        let data = r#"
        {
            "error": "Short description of the error",
            "errorMessage": "Longer description which can be shown to the user"
        }"#;

        let v: Error = serde_json::from_str(data).unwrap();

        assert_eq!(
            v,
            Error {
                error: "Short description of the error".to_string(),
                error_message: "Longer description which can be shown to the user".to_string(),
                cause: None,
            }
        )
    }
}
