pub use super::token::{AccessToken, ClientToken};
use serde::{Deserialize, Serialize};

/// Checks if an accessToken is usable for authentication with a Minecraft server.
///
/// The Minecraft Launcher (as of version 1.6.13) calls this endpoint on startup to verify that its
/// saved token is still usable, and calls /refresh if this returns an error.
///
/// Endpoint `authserver/validate`
///
/// **Note**: that an accessToken may be unusable for authentication with a Minecraft server, but
/// still be good enough for /refresh. This mainly happens when one has used another client (e.g.
/// played Minecraft on another PC with the same account). It seems only the most recently obtained
///  accessToken for a given account can reliably be used for authentication (the next-to-last
/// token also seems to remain valid, but don't rely on it).
///
/// `/validate` may be called with or without a clientToken. If a clientToken is provided, it should match the one used to obtain the accessToken. The Minecraft Launcher does send a clientToken to /validate.
#[derive(Deserialize, Serialize, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[serde(rename_all = "camelCase")]
pub struct ValidatePayload {
    /// hexadecimal or JSON-Web-Token (unconfirmed) [The normal accessToken can be found in the
    /// payload of the JWT (second by '.' separated part as Base64 encoded JSON object), in key
    /// "yggt"]
    pub access_token: AccessToken,
    /// This needs to be identical to the one used to obtain the accessToken in the first place
    pub client_token: Option<ClientToken>,
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_validate_payload() {
        let data = r#"
        {
            "accessToken": "valid accessToken",
            "clientToken": "client identifier"
        }"#;

        let v: ValidatePayload = serde_json::from_str(data).unwrap();

        assert_eq!(
            v,
            ValidatePayload {
                access_token: "valid accessToken".to_string(),
                client_token: Some("client identifier".to_string()),
            }
        )
    }

    #[test]
    fn test_validate_payload_no_client() {
        let data = r#"
        {
            "accessToken": "valid accessToken"
        }"#;

        let v: ValidatePayload = serde_json::from_str(data).unwrap();

        assert_eq!(
            v,
            ValidatePayload {
                access_token: "valid accessToken".to_string(),
                client_token: None,
            }
        )
    }
}
