use uuid::Uuid;

/// hexadecimal or JSON-Web-Token (unconfirmed) [The normal accessToken can be found in the
/// payload of the JWT (second by '.' separated part as Base64 encoded JSON object), in key
/// "yggt"]
pub type AccessToken = String;

/// The clientToken should be a randomly generated identifier and must be identical for each
/// request. The vanilla launcher generates a random (version 4) UUID on first run and saves
/// it, reusing it for every subsequent request. In case it is omitted the server will
/// generate a random token based on Java's UUID.toString() which should then be stored by
/// the client. This will however also invalidate all previously acquired accessTokens for
/// this user across all clients.
pub type ClientToken = String;

/// Generate random new client token based on UUID.
pub fn new_client_token() -> ClientToken {
    Uuid::new_v4().to_string()
}

/// Update (1.7.x): The server ID is now sent as an empty string. Hashes also utilize the public
/// key, so they will still be correct.
///
/// Pre-1.7.x: The server ID string is a randomly-generated string of characters with a maximum
/// length of 20 code points (the client disconnects with an exception if the length is longer than 20).
///
/// The client appears to arrive at incorrect hashes if the server ID string contains certain
/// unprintable characters, so for consistent results only characters with code points in the
/// range U+0021-U+007E (inclusive) should be sent. This range corresponds to all of ASCII with
/// the exception of the space character (U+0020) and all control characters (U+0000-U+001F, U+007F).
///
/// The client appears to arrive at incorrect hashes if the server ID string is too short. 15 to
/// 20 (inclusive) length strings have been observed from the Notchian server and confirmed to
/// work as of 1.5.2.
pub type ServerId = String;
