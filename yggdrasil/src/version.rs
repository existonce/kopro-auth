use semver::Version;
use serde::{de, Deserialize, Deserializer, Serialize, Serializer};

pub fn serialize<S>(v: &Option<Version>, s: S) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    Serialize::serialize(&v.as_ref().map(|a| a.to_string()).as_deref(), s)
}

pub fn deserialize<'de, D>(d: D) -> Result<Option<Version>, D::Error>
where
    D: Deserializer<'de>,
{
    let mstr: Option<String> = Deserialize::deserialize(d)?;
    match mstr {
        None => Ok(None),
        Some(str) => Ok(Some(Version::parse(&str).map_err(de::Error::custom)?)),
    }
}
