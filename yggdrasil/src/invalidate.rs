pub use super::token::{AccessToken, ClientToken};
use serde::{Deserialize, Serialize};

/// Invalidates accessTokens using a client/access token pair.
///
/// Endpoint `authserver/invalidate`
///
#[derive(Deserialize, Serialize, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[serde(rename_all = "camelCase")]
pub struct InvalidatePayload {
    /// hexadecimal or JSON-Web-Token (unconfirmed) [The normal accessToken can be found in the
    /// payload of the JWT (second by '.' separated part as Base64 encoded JSON object), in key
    /// "yggt"]
    pub access_token: AccessToken,
    /// This needs to be identical to the one used to obtain the accessToken in the first place
    pub client_token: ClientToken,
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_invalidate_payload() {
        let data = r#"
        {
            "accessToken": "valid accessToken",
            "clientToken": "client identifier"
        }"#;

        let v: InvalidatePayload = serde_json::from_str(data).unwrap();

        assert_eq!(
            v,
            InvalidatePayload {
                access_token: "valid accessToken".to_string(),
                client_token: "client identifier".to_string(),
            }
        )
    }
}
