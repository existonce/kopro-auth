use serde::{Deserialize, Serialize};

/// Changes user password on server.
///
/// Provide current password and new one for registration.
///
/// Endpoint `authserver/change-password`
///
/// **Note**: Not part of offical protocol!
#[derive(Deserialize, Serialize, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[serde(rename_all = "camelCase")]
pub struct ChangePasswordPayload {
    pub username: String,
    pub old_password: String,
    pub new_password: String,
}

#[derive(Deserialize, Serialize, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[serde(untagged)]
pub enum ChangePassword {
    Ok(()),
    Err(super::error::Error),
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_payload() {
        let data = r#"
        {
            "username": "mojang account name",
            "oldPassword": "mojang account password",
            "newPassword": "new mojang account password"
        }"#;

        let v: ChangePasswordPayload = serde_json::from_str(data).unwrap();

        assert_eq!(
            v,
            ChangePasswordPayload {
                username: "mojang account name".to_string(),
                old_password: "mojang account password".to_string(),
                new_password: "new mojang account password".to_string(),
            }
        )
    }
}
