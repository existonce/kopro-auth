use serde::{Deserialize, Serialize};
use uuid::Uuid;

//// UUID of profile without dashes
pub type ProfileId = String;

/// Part of response for [`Authenticate`]
#[derive(Deserialize, Serialize, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Clone)]
#[serde(rename_all = "camelCase")]
pub struct UserProfile {
    /// Presumably same value as before
    pub agent: Option<String>,
    /// hexadecimal
    pub id: ProfileId,
    pub name: String,
    /// hexadecimal
    pub user_id: Option<String>,
    /// Milliseconds since Jan 1 1970
    pub created_at: Option<u64>,
    /// Present even when false
    pub legacy_profile: Option<bool>,
    /// probably false
    pub suspended: Option<bool>,
    /// probably true
    pub paid: Option<bool>,
    /// Seems to be false even for migrated accounts...?  (https://bugs.mojang.com/browse/WEB-1461)
    pub migrated: Option<bool>,
    /// Only appears in the response if true. Default to false.  Redundant to the newer legacyProfile...
    pub legacy: Option<bool>,
}

impl UserProfile {
    /// Create stub profile for offline startup with given username
    pub fn offline(username: &str) -> UserProfile {
        UserProfile {
            agent: None,
            id: Uuid::new_v4().to_string(),
            name: username.to_owned(),
            user_id: None,
            created_at: None,
            legacy_profile: None,
            suspended: None,
            paid: None,
            migrated: None,
            legacy: None,
        }
    }
}

/// Part of response for [`Authenticate`]
#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Clone)]
pub enum UserProperty {
    /// might not be present for all accounts. Java locale format (https://docs.oracle.com/javase/8/docs/api/java/util/Locale.html#toString--)
    PreferedLocale(String),
    /// only present if a twitch account is associated (see https://account.mojang.com/me/settings).
    /// OAuth 2.0 Token; alphanumerical; e.g. https://api.twitch.tv/kraken?oauth_token=[...]
    /// the Twitch API is documented here: https://github.com/justintv/Twitch-API
    TwichToken(String),
    Unknown(String, String),
}

impl Serialize for UserProperty {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        #[derive(Serialize)]
        struct RawProperty {
            name: String,
            value: String,
        }

        let raw = match self {
            UserProperty::PreferedLocale(value) => RawProperty {
                name: "preferredLanguage".to_string(),
                value: value.clone(),
            },
            UserProperty::TwichToken(value) => RawProperty {
                name: "twitch_access_token".to_string(),
                value: value.clone(),
            },
            UserProperty::Unknown(name, value) => RawProperty {
                name: name.clone(),
                value: value.clone(),
            },
        };

        raw.serialize(serializer)
    }
}

impl<'a> Deserialize<'a> for UserProperty {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'a>,
    {
        #[derive(Deserialize)]
        struct RawProperty {
            name: String,
            value: String,
        }

        let raw = RawProperty::deserialize(deserializer)?;

        match &raw.name[..] {
            "preferredLanguage" => Ok(UserProperty::PreferedLocale(raw.value)),
            "twitch_access_token" => Ok(UserProperty::TwichToken(raw.value)),
            _ => Ok(UserProperty::Unknown(raw.name, raw.value)),
        }
    }
}

/// Part of response for [`Authenticate`]
#[derive(Deserialize, Serialize, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Clone)]
#[serde(rename_all = "camelCase")]
pub struct User {
    /// hexadecimal
    pub id: String,
    /// Hashed(?) value for unmigrated accounts
    pub email: Option<String>,
    /// Regular name for unmigrated accounts, email for migrated ones
    pub username: Option<String>,
    /// IP address with the last digit censored
    pub register_ip: Option<String>,
    pub migrated_from: Option<String>,
    /// Milliseconds since Jan 1 1970
    pub migrated_at: Option<u64>,
    /// /// Milliseconds since Jan 1 1970. May be a few minutes earlier than createdAt for profile
    pub registered_at: Option<u64>,
    /// Milliseconds since Jan 1 1970
    pub password_changed_at: Option<u64>,
    /// Milliseconds since Jan 1 1970
    pub date_of_birth: Option<i64>,
    pub suspended: Option<bool>,
    pub blocked: Option<bool>,
    pub secured: Option<bool>,
    /// Seems to be false even when migratedAt and migratedFrom are present...
    pub migrated: Option<bool>,
    pub email_verified: Option<bool>,
    pub legacy_user: Option<bool>,
    pub verified_by_parent: Option<bool>,
    pub properties: Option<Vec<UserProperty>>,
}
