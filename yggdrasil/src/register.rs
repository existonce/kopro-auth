use serde::{Deserialize, Serialize};

/// Creates new user on server.
///
/// If signupt successfull the resturn code is 200 or error otherwise.
///
/// Endpoint `authserver/register`
///
/// **Note**: Not part of offical protocol!
#[derive(Deserialize, Serialize, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[serde(rename_all = "camelCase")]
pub struct RegisterPayload {
    pub username: String,
    pub password: String,
}

#[derive(Deserialize, Serialize, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[serde(untagged)]
pub enum RegisterResp {
    Ok(()),
    Err(super::error::Error),
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_payload() {
        let data = r#"
        {
            "username": "mojang account name",
            "password": "mojang account password"
        }"#;

        let v: RegisterPayload = serde_json::from_str(data).unwrap();

        assert_eq!(
            v,
            RegisterPayload {
                username: "mojang account name".to_string(),
                password: "mojang account password".to_string(),
            }
        )
    }
}
