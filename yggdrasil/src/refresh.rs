use serde::{Deserialize, Serialize};

pub use super::profile::{User, UserProfile, UserProperty};
pub use super::token::{AccessToken, ClientToken};

/// Refreshes a valid accessToken. It can be used to keep a user logged in between gaming sessions and is preferred over storing the user's password in a file.
///
/// Endpoint `authserver/refresh`
///
/// **Note**:  The provided accessToken gets invalidated.
#[derive(Deserialize, Serialize, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[serde(rename_all = "camelCase")]
pub struct RefreshPayload {
    /// hexadecimal or JSON-Web-Token (unconfirmed) [The normal accessToken can be found in the
    /// payload of the JWT (second by '.' separated part as Base64 encoded JSON object), in key
    /// "yggt"]
    pub access_token: AccessToken,
    /// This needs to be identical to the one used to obtain the accessToken in the first place
    pub client_token: ClientToken,
    /// optional; sending it will result in an error
    pub selected_profile: Option<UserProfile>,
    ///optional; default: false; true adds the user object to the response
    pub request_user: Option<bool>,
}

#[derive(Deserialize, Serialize, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[serde(rename_all = "camelCase")]
pub struct Refresh {
    /// hexadecimal or JSON-Web-Token (unconfirmed) [The normal accessToken can be found in the
    /// payload of the JWT (second by '.' separated part as Base64 encoded JSON object), in key
    /// "yggt"]
    pub access_token: AccessToken,
    /// identical to the one received
    pub client_token: ClientToken,
    pub selected_profile: Option<UserProfile>,
    /// only present if requestUser was true in the request payload
    pub user: Option<User>,
}

#[derive(Deserialize, Serialize, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[serde(untagged)]
pub enum RefreshResp {
    Ok(Refresh),
    Err(super::error::Error),
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_refresh_payload() {
        let data = r#"
        {
            "accessToken": "valid accessToken",
            "clientToken": "client identifier",

            "selectedProfile": {
                "id": "profile identifier",
                "name": "player name"
            },
            "requestUser": true
        }"#;

        let v: RefreshPayload = serde_json::from_str(data).unwrap();

        assert_eq!(
            v,
            RefreshPayload {
                access_token: "valid accessToken".to_string(),
                client_token: "client identifier".to_string(),
                selected_profile: Some(UserProfile {
                    agent: None,
                    id: "profile identifier".to_string(),
                    name: "player name".to_string(),
                    user_id: None,
                    created_at: None,
                    legacy_profile: None,
                    suspended: None,
                    paid: None,
                    migrated: None,
                    legacy: None,
                }),
                request_user: Some(true),
            }
        )
    }

    #[test]
    fn test_refresh_response() {
        let data = r#"
        {
            "accessToken": "random access token",
            "clientToken": "client identifier",
            "selectedProfile": {
                "id": "profile identifier",
                "name": "player name"
            },
            "user": {
                "id": "user identifier",
                "properties": [
                    {
                        "name": "preferredLanguage",
                        "value": "en"
                    },
                    {
                        "name": "twitch_access_token",
                        "value": "twitch oauth token"
                    }
                ]
            }
        }"#;

        let v: Refresh = serde_json::from_str(data).unwrap();

        assert_eq!(
            v,
            Refresh {
                access_token: "random access token".to_string(),
                client_token: "client identifier".to_string(),
                selected_profile: Some(UserProfile {
                    agent: None,
                    id: "profile identifier".to_string(),
                    name: "player name".to_string(),
                    user_id: None,
                    created_at: None,
                    legacy_profile: None,
                    suspended: None,
                    paid: None,
                    migrated: None,
                    legacy: None,
                }),
                user: Some(User {
                    id: "user identifier".to_string(),
                    email: None,
                    username: None,
                    register_ip: None,
                    migrated_from: None,
                    migrated_at: None,
                    registered_at: None,
                    password_changed_at: None,
                    date_of_birth: None,
                    suspended: None,
                    blocked: None,
                    secured: None,
                    migrated: None,
                    email_verified: None,
                    legacy_user: None,
                    verified_by_parent: None,
                    properties: Some(vec![
                        UserProperty::PreferedLocale("en".to_string()),
                        UserProperty::TwichToken("twitch oauth token".to_string()),
                    ]),
                }),
            }
        )
    }
}
