use serde::{Deserialize, Serialize};

pub use super::profile::{User, UserProfile, UserProperty};
pub use super::token::{AccessToken, ClientToken};

/// URL of official minecraft auth server
pub const OFFICIAL_SERVER: &str = "https://authserver.mojang.com";

/// User agent in ['AuthenticatePayload']
#[derive(Deserialize, Serialize, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[serde(rename_all = "camelCase")]
pub struct Agent {
    /// For Mojang's other game Scrolls, "Scrolls" should be used
    pub name: String,
    /// This number might be increased by the vanilla client in the future
    pub version: u32,
}

impl Default for Agent {
    fn default() -> Self {
        Agent {
            name: "Minecraft".to_string(),
            version: 1,
        }
    }
}

/// Authenticates a user using their password.
///
/// Endpoint `authserver/authenticate`
#[derive(Deserialize, Serialize, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[serde(rename_all = "camelCase")]
pub struct AuthenticatePayload {
    pub agent: Option<Agent>,
    pub username: String,
    pub password: String,
    /// The clientToken should be a randomly generated identifier and must be identical for each
    /// request. The vanilla launcher generates a random (version 4) UUID on first run and saves
    /// it, reusing it for every subsequent request. In case it is omitted the server will
    /// generate a random token based on Java's UUID.toString() which should then be stored by
    /// the client. This will however also invalidate all previously acquired accessTokens for
    /// this user across all clients.
    pub client_token: Option<ClientToken>,
    pub request_user: Option<bool>,
}

/// Authenticates a user using their password.
///
/// Endpoint `authserver/authenticate`
///
/// **Note**: If a user wishes to stay logged in on their computer you are strongly advised to
/// store the received accessToken instead of the password itself.
///
/// Currently each account will only have one single profile, multiple profiles per account are
/// however planned in the future. If a user attempts to log into a valid Mojang account with no
/// attached Minecraft license, the authentication will be successful, but the response will not
/// contain a selectedProfile field, and the availableProfiles array will be empty.
///
/// Some instances in the wild have been observed of Mojang returning a flat null for failed
/// refresh attempts against legacy accounts. It's not clear what the actual error tied to the
/// null response is and it is extremely rare, but implementations should be wary of null output
/// from the response.
///
/// This endpoint is severely rate-limited: multiple /authenticate requests for the same account
/// in a short amount of time (think 3 requests in a few seconds), even with the correct password,
/// will eventually lead to an Invalid credentials. response. This error clears up a few seconds
/// later.
#[derive(Deserialize, Serialize, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[serde(rename_all = "camelCase")]
pub struct Authenticate {
    /// hexadecimal or JSON-Web-Token (unconfirmed) [The normal accessToken can be found in the
    /// payload of the JWT (second by '.' separated part as Base64 encoded JSON object), in key
    /// "yggt"]
    pub access_token: AccessToken,
    /// identical to the one received
    pub client_token: ClientToken,
    /// only present if the agent field was received
    pub available_profiles: Option<Vec<UserProfile>>,
    /// only present if the agent field was received
    pub selected_profile: Option<UserProfile>,
    /// only present if requestUser was true in the request payload
    pub user: Option<User>,
}

#[derive(Deserialize, Serialize, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[serde(untagged)]
pub enum AuthenticateResp {
    Ok(Authenticate),
    Err(super::error::Error),
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_auth_payload() {
        let data = r#"
        {
            "agent": {
                "name": "Minecraft",
                "version": 1
            },
            "username": "mojang account name",
            "password": "mojang account password",
            "clientToken": "client identifier",
            "requestUser": true
        }"#;

        let v: AuthenticatePayload = serde_json::from_str(data).unwrap();

        assert_eq!(
            v,
            AuthenticatePayload {
                agent: Some(Agent::default()),
                username: "mojang account name".to_string(),
                password: "mojang account password".to_string(),
                client_token: Some("client identifier".to_string()),
                request_user: Some(true),
            }
        )
    }

    #[test]
    fn test_auth_resp() {
        let data = r#"
        {
            "accessToken": "random access token",
            "clientToken": "client identifier",
            "availableProfiles": [
                {
                    "agent": "minecraft",
                    "id": "profile identifier",
                    "name": "player name",
                    "userId": "hex string",
                    "createdAt": 1325376000000,
                    "legacyProfile": true,
                    "suspended": true,
                    "paid": true,
                    "migrated": true,
                    "legacy": true
                }
            ],
            "selectedProfile": {
                "id": "uuid without dashes",
                "name": "player name",
                "userId": "hex string",
                "createdAt": 1325376000000,
                "legacyProfile": true,
                "suspended": true,
                "paid": true,
                "migrated": true,
                "legacy": true
            },
            "user": {
                "id": "user identifier",
                "email": "user@email.example",
                "username": "user@email.example",
                "registerIp": "198.51.100.*",
                "migratedFrom": "minecraft.net",
                "migratedAt": 1420070400000,
                "registeredAt": 1325376000000,
                "passwordChangedAt": 1569888000000,
                "dateOfBirth": -2208988800000,
                "suspended": false,
                "blocked": false,
                "secured": true,
                "migrated": false,
                "emailVerified": true,
                "legacyUser": false,
                "verifiedByParent": false,
                "properties": [
                    {
                        "name": "preferredLanguage",
                        "value": "en"
                    },
                    {
                        "name": "twitch_access_token",
                        "value": "twitch oauth token"
                    }
                ]
            }
        }"#;

        let v: Authenticate = serde_json::from_str(data).unwrap();

        assert_eq!(
            v,
            Authenticate {
                access_token: "random access token".to_string(),
                client_token: "client identifier".to_string(),
                available_profiles: Some(vec![UserProfile {
                    agent: Some("minecraft".to_string()),
                    id: "profile identifier".to_string(),
                    name: "player name".to_string(),
                    user_id: Some("hex string".to_string()),
                    created_at: Some(1325376000000),
                    legacy_profile: Some(true),
                    suspended: Some(true),
                    paid: Some(true),
                    migrated: Some(true),
                    legacy: Some(true),
                }]),
                selected_profile: Some(UserProfile {
                    agent: None,
                    id: "uuid without dashes".to_string(),
                    name: "player name".to_string(),
                    user_id: Some("hex string".to_string()),
                    created_at: Some(1325376000000),
                    legacy_profile: Some(true),
                    suspended: Some(true),
                    paid: Some(true),
                    migrated: Some(true),
                    legacy: Some(true),
                }),
                user: Some(User {
                    id: "user identifier".to_string(),
                    email: Some("user@email.example".to_string()),
                    username: Some("user@email.example".to_string()),
                    register_ip: Some("198.51.100.*".to_string()),
                    migrated_from: Some("minecraft.net".to_string()),
                    migrated_at: Some(1420070400000),
                    registered_at: Some(1325376000000),
                    password_changed_at: Some(1569888000000),
                    date_of_birth: Some(-2208988800000),
                    suspended: Some(false),
                    blocked: Some(false),
                    secured: Some(true),
                    migrated: Some(false),
                    email_verified: Some(true),
                    legacy_user: Some(false),
                    verified_by_parent: Some(false),
                    properties: Some(vec![
                        UserProperty::PreferedLocale("en".to_string()),
                        UserProperty::TwichToken("twitch oauth token".to_string()),
                    ]),
                }),
            }
        )
    }
}
