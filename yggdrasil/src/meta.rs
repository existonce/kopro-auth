use do_notation::m;
use semver::Version;
use serde::{Deserialize, Serialize};

/// The following API is designed to facilitate automatic configuration of authlib-injector.
///
/// Endpoint `/`
#[derive(Deserialize, Serialize, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Clone)]
#[serde(rename_all = "camelCase")]
pub struct MetaPayload {
    /// The content in is not mandatory, and its fields are optional.
    pub meta: ServerMeta,

    /// Minecraft will only download textures from domains in the whitelist. If the domain name of
    /// the material URL is not in the whitelist, it will appear Textures payload has been tampered
    /// with (non-whitelisted domain)error. Refer to for the reasons for adopting this mechanism
    /// MC-78491 .
    ///
    /// The material whitelist contains by default .minecraft.net、 .mojang.com
    /// Two rules, you can set skinDomainsAttribute to add additional whitelist rules.
    /// The rule format is as follows:
    ///
    /// * If the rule is .(Dot), matches the domain name ending with this rule. E.g .example.com
    ///   match a.example.com、 b.a.example.com, Does not match example.com。
    ///
    /// * If the rules are not .(Dot), the matched domain name must be rule exactly the same as the .
    ///   E.g example.com match example.com, Does not match a.example.com、 eexample.com。
    pub skin_domains: Vec<String>,

    /// A public key in PEM format, used to verify the digital signature
    /// of the role attribute. Which is -----BEGIN PUBLIC KEY----- Starts with
    /// -----END PUBLIC KEY----- At the end, a newline character is allowed in the middle, but
    /// other blank characters are not allowed (a newline character is also allowed at the end of
    /// the text).
    pub signature_publickey: String,
}

impl Default for MetaPayload {
    fn default() -> Self {
        MetaPayload {
            meta: ServerMeta::default(),
            skin_domains: vec![],
            signature_publickey: DEFAULT_PUBLIC_KEY.to_owned(),
        }
    }
}

impl MetaPayload {
    fn with_link<F>(&self, f: F) -> Option<&str>
    where
        F: FnOnce(&ServerLinks) -> Option<&PlatformLinks>,
    {
        m! {
            ls <- self.meta.links.as_ref();
            pl <- f(ls);
            c <- pl.by_current();
            return c;
        }
    }

    pub fn launcher_link(&self) -> Option<&str> {
        self.with_link(|ls| ls.launcher.as_ref())
    }

    pub fn client_link(&self) -> Option<&str> {
        self.with_link(|ls| ls.client.as_ref())
    }

    /// Encode self in base 64 encoding. Used to pass prefetched meta information to client.
    pub fn base64(&self) -> Result<String, serde_json::Error> {
        Ok(base64::encode(serde_json::to_string(self)?))
    }
}

#[derive(Deserialize, Serialize, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Clone)]
#[serde(rename_all = "camelCase")]
pub struct ServerMeta {
    /// server nickname
    pub server_name: Option<String>,
    /// The name of the server implementation
    pub implementation_name: Option<String>,
    /// Server version
    pub implementation_version: Option<String>,
    /// If you need to display the verification server homepage address, registration page address
    /// and other information in the launcher, you can meta Add one links Field.
    pub links: Option<ServerLinks>,
    /// Latest version of launcher in semver format
    #[serde(with = "crate::version")]
    pub launcher_version: Option<Version>,
    /// Latest client version in semver format
    #[serde(with = "crate::version")]
    pub client_version: Option<Version>,
}

impl Default for ServerMeta {
    fn default() -> Self {
        ServerMeta {
            server_name: Some("Koprokubach".to_owned()),
            implementation_name: Some("yggdrasil-rust".to_owned()),
            implementation_version: Some("0.1".to_owned()),
            links: Some(ServerLinks::default()),
            launcher_version: Some("1.7.0".parse().unwrap()),
            client_version: Some("1.6.0".parse().unwrap()),
        }
    }
}

/// Links that are different by client current platform
#[derive(Deserialize, Serialize, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Clone)]
#[serde(rename_all = "camelCase")]
pub struct PlatformLinks {
    linux: String,
    windows: String,
    mac: String,
}

impl PlatformLinks {
    pub fn from_template(url: &str) -> Self {
        PlatformLinks {
            linux: url.replace("{}", "linux"),
            windows: url.replace("{}", "windows"),
            mac: url.replace("{}", "mac"),
        }
    }

    pub fn by_current(&self) -> Option<&str> {
        let os = std::env::consts::OS;
        if os == "linux" {
            Some(&self.linux)
        } else if os == "windows" {
            Some(&self.windows)
        } else if os == "macos" {
            Some(&self.mac)
        } else {
            None
        }
    }
}

/// If you need to display the verification server homepage address, registration page address
/// and other information in the launcher, you can meta Add one links Field.
#[derive(Deserialize, Serialize, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Clone)]
#[serde(rename_all = "camelCase")]
pub struct ServerLinks {
    /// Verify server homepage address
    pub homepage: Option<String>,
    /// Register page address
    pub register: Option<String>,
    pub launcher: Option<PlatformLinks>,
    pub client: Option<PlatformLinks>,
}

impl ServerLinks {
    pub fn new() -> Self {
        ServerLinks {
            homepage: None,
            register: None,
            launcher: None,
            client: None,
        }
    }
}

impl Default for ServerLinks {
    fn default() -> Self {
        ServerLinks {
            homepage: Some("https://kubach.day".to_owned()),
            register: Some("https://kubach.day/register".to_owned()),
            launcher: Some(PlatformLinks {
                linux: "https://kubach.day/static/launcher-linux.zip".to_owned(),
                windows: "https://kubach.day/static/launcher-windows.zip".to_owned(),
                mac: "https://kubach.day/static/lanucher-mac.zip".to_owned(),
            }),
            client: Some(PlatformLinks {
                linux: "https://kubach.day/static/client-linux.zip".to_owned(),
                windows: "https://kubach.day/static/client-windows.zip".to_owned(),
                mac: "https://kubach.day/static/client-mac.zip".to_owned(),
            }),
        }
    }
}

/// Got by `ssh-keygen -e -m pem -f $1 | openssl rsa -RSAPublicKey_in > id_rsa.pub.pem`
const DEFAULT_PUBLIC_KEY: &str = r"
-----BEGIN PUBLIC KEY-----
MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEA0aleSAhhddCASAtyQV6O
ASfcwN4gyKvTiGXHRR3k5WoP9CdVcyfqjeY/MzGytqdYCrCaKXo0nEx4aEcYgHe6
cHE7SSuwg9PxonpLsZPBd9Tqq9WKI+bNG70zWrzSdyxqU9aazDUStc3fH53LqEJE
KMvXN323oW90tO0jt3kuOuTjePKDXn+BxOUwg/OnUf0lp6uMBLjJGU/s87P4kp3N
BLEpm96J6lwsqaIu6MpbZEMsdD0/tuMKGyDxvWzW5K8388cMmtyM2qo4vwTXNT+8
mjgdNFZ5wLpAqJnVONycohrkBU73fQrc+MmCHCE3jpsSpIGatlxdF0vWZ6c+bN2k
14Qw0y52DAearfj5R2pDkX9uZy8qlBs1MvylXjvagpOv/d3FqjudEs9SJRj6qbB7
hjKE4pXiFH8xaK06BsRIcnvhO/xcE+8BBhd2nGKrGlfBvWUznes/Ug2tc3gDYk1f
45qw32EZi1drHDPD5elcmke8nqehWN+/+I6FjVJw52NyIlJ6abI40Keo6vxSuKhx
bQZ0ytQlReTH1t91sOi4hNEMcOko+yZC4rkUS+jwVhPEOlVhtxS5e28O8pHSaiyi
pCrNwQEYY2rfXsPZdkkiNvppbZNc5TiaH25RpcspUgTIpxGZu1rgiRqBzteM2P1i
ngQs6PRYtiN+MCp1aB6jZYMCAwEAAQ==
-----END PUBLIC KEY-----
";

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_meta_payload() {
        let data = r#"
        {
          "meta": {
            "serverName": "Koprokubach",
            "implementationName": "yggdrasil-rust",
            "implementationVersion": "0.1",
            "links": {
              "homepage": "https://kubach.day",
              "register": "https://kubach.day/register",
              "launcher": null,
              "client": null
            },
            "launcherVersion": "1.0.0",
            "clientVersion": "1.0.0"
          },
          "skinDomains": [],
          "signaturePublickey": "\n-----BEGIN PUBLIC KEY-----\nMIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEA0aleSAhhddCASAtyQV6O\nASfcwN4gyKvTiGXHRR3k5WoP9CdVcyfqjeY/MzGytqdYCrCaKXo0nEx4aEcYgHe6\ncHE7SSuwg9PxonpLsZPBd9Tqq9WKI+bNG70zWrzSdyxqU9aazDUStc3fH53LqEJE\nKMvXN323oW90tO0jt3kuOuTjePKDXn+BxOUwg/OnUf0lp6uMBLjJGU/s87P4kp3N\nBLEpm96J6lwsqaIu6MpbZEMsdD0/tuMKGyDxvWzW5K8388cMmtyM2qo4vwTXNT+8\nmjgdNFZ5wLpAqJnVONycohrkBU73fQrc+MmCHCE3jpsSpIGatlxdF0vWZ6c+bN2k\n14Qw0y52DAearfj5R2pDkX9uZy8qlBs1MvylXjvagpOv/d3FqjudEs9SJRj6qbB7\nhjKE4pXiFH8xaK06BsRIcnvhO/xcE+8BBhd2nGKrGlfBvWUznes/Ug2tc3gDYk1f\n45qw32EZi1drHDPD5elcmke8nqehWN+/+I6FjVJw52NyIlJ6abI40Keo6vxSuKhx\nbQZ0ytQlReTH1t91sOi4hNEMcOko+yZC4rkUS+jwVhPEOlVhtxS5e28O8pHSaiyi\npCrNwQEYY2rfXsPZdkkiNvppbZNc5TiaH25RpcspUgTIpxGZu1rgiRqBzteM2P1i\nngQs6PRYtiN+MCp1aB6jZYMCAwEAAQ==\n-----END PUBLIC KEY-----\n"
        }"#;

        let v: MetaPayload = serde_json::from_str(data).unwrap();

        assert_eq!(
            v,
            MetaPayload {
                meta: ServerMeta {
                    server_name: Some("Koprokubach".to_owned()),
                    implementation_name: Some("yggdrasil-rust".to_owned()),
                    implementation_version: Some("0.1".to_owned()),
                    links: Some(ServerLinks {
                        homepage: Some("https://kubach.day".to_owned()),
                        register: Some("https://kubach.day/register".to_owned()),
                        launcher: None,
                        client: None,
                    }),
                    launcher_version: Some("1.0.0".parse().unwrap()),
                    client_version: Some("1.0.0".parse().unwrap()),
                },
                skin_domains: vec![],
                signature_publickey: DEFAULT_PUBLIC_KEY.to_owned(),
            }
        )
    }
}
