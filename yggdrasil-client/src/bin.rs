extern crate base64;
extern crate clap;
extern crate serde_json;
extern crate tokio;
extern crate yggdrasil_client;

use clap::Parser;
use std::error::Error;
use yggdrasil_client::endpoints::*;

#[derive(Parser, Debug)]
#[clap(version, author = "schoolgirl <existonce@protonmail.com>")]
struct Opts {
    #[clap(short, long, default_value = "https://auth.kubach.day")]
    server: String,
    #[clap(subcommand)]
    subcmd: SubCommand,
}

#[derive(Parser, Debug)]
enum SubCommand {
    Auth(AuthCmd),
    Refresh(RefreshCmd),
    Validate(ValidateCmd),
    Invalidate(InvalidateCmd),
    Signout(SignoutCmd),
    Register(RegisterCmd),
    ChangePassword(ChangePasswordCmd),
    Meta(MetaCmd),
}

#[derive(Parser, Debug)]
struct AuthCmd {
    #[clap(short, long)]
    user: String,
    #[clap(short, long)]
    password: String,
    #[clap(
        short,
        long,
    )]
    /// "Optional client id. In case it is omitted the server will generate new random token and invalidate all old ones."
    client_token: Option<String>,
}

#[derive(Parser, Debug)]
struct RefreshCmd {
    #[clap(short, long)]
    access_token: String,
    #[clap(short, long)]
    client_token: String,
}

#[derive(Parser, Debug)]
struct ValidateCmd {
    #[clap(short, long)]
    access_token: String,
    #[clap(short, long)]
    client_token: Option<String>,
}

#[derive(Parser, Debug)]
struct InvalidateCmd {
    #[clap(short, long)]
    access_token: String,
    #[clap(short, long)]
    client_token: String,
}

#[derive(Parser, Debug)]
struct SignoutCmd {
    #[clap(short, long)]
    user: String,
    #[clap(short, long)]
    password: String,
}

#[derive(Parser, Debug)]
struct RegisterCmd {
    #[clap(short, long)]
    user: String,
    #[clap(short, long)]
    password: String,
}

#[derive(Parser, Debug)]
struct ChangePasswordCmd {
    #[clap(short, long)]
    user: String,
    #[clap(short, long)]
    old_password: String,
    #[clap(short, long)]
    new_password: String,
}

#[derive(Parser, Debug)]
struct MetaCmd {
    #[clap(short, long)]
    /// Output in json format
    json: bool,
    #[clap(short, long)]
    /// Output in json format encoded in base64
    base64: bool,
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    let opts: Opts = Opts::parse();

    match opts.subcmd {
        SubCommand::Auth(cmd) => {
            let res: AuthenticateResp = authenticate_simple(
                &opts.server,
                &cmd.user,
                &cmd.password,
                cmd.client_token.as_deref(),
            )
            .await?;
            println!("{:?}", res);
        }
        SubCommand::Refresh(cmd) => {
            let res: RefreshResp =
                refresh_simple(&opts.server, &cmd.access_token, &cmd.client_token).await?;
            println!("{:?}", res);
        }
        SubCommand::Validate(cmd) => {
            let res: bool =
                validate(&opts.server, &cmd.access_token, cmd.client_token.as_deref()).await?;
            println!("{:?}", res);
        }
        SubCommand::Invalidate(cmd) => {
            let res: bool = invalidate(&opts.server, &cmd.access_token, &cmd.client_token).await?;
            println!("{:?}", res);
        }
        SubCommand::Signout(cmd) => {
            let res: bool = signout(&opts.server, &cmd.user, &cmd.password).await?;
            println!("{:?}", res);
        }
        SubCommand::Register(cmd) => {
            let res: RegisterResp = register(&opts.server, &cmd.user, &cmd.password).await?;
            println!("{:?}", res);
        }
        SubCommand::ChangePassword(cmd) => {
            let res: ChangePassword = change_password(
                &opts.server,
                &cmd.user,
                &cmd.old_password,
                &cmd.new_password,
            )
            .await?;
            println!("{:?}", res);
        }
        SubCommand::Meta(cmd) => {
            let res: MetaPayload = server_meta(&opts.server).await?;
            if cmd.json {
                let js = serde_json::to_string(&res)?;
                println!("{}", js);
            } else if cmd.base64 {
                let js = serde_json::to_string(&res)?;
                let b64 = base64::encode(&js);
                println!("{}", b64);
            } else {
                println!("{:?}", res);
            }
        }
    }
    Ok(())
}
