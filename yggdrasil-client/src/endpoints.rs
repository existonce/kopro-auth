pub use yggdrasil::auth::*;
pub use yggdrasil::change::*;
pub use yggdrasil::error::*;
pub use yggdrasil::invalidate::*;
pub use yggdrasil::meta::*;
pub use yggdrasil::refresh::*;
pub use yggdrasil::register::*;
pub use yggdrasil::signout::*;
pub use yggdrasil::validate::*;

pub async fn authenticate(
    server: &str,
    payload: AuthenticatePayload,
) -> Result<AuthenticateResp, reqwest::Error> {
    let client = reqwest::Client::new();
    let endpoint = format!("{}/{}", server, "authserver/authenticate");
    let raw_res = client.post(endpoint).json(&payload).send().await?;
    Ok(raw_res.json().await?)
}

pub async fn authenticate_simple(
    server: &str,
    username: &str,
    password: &str,
    client_token: Option<&str>,
) -> Result<AuthenticateResp, reqwest::Error> {
    let payload = AuthenticatePayload {
        agent: Some(Agent::default()),
        username: username.to_string(),
        password: password.to_string(),
        client_token: client_token.map(|s| s.to_string()),
        request_user: Some(true),
    };
    Ok(authenticate(server, payload).await?)
}

pub async fn refresh(server: &str, payload: RefreshPayload) -> Result<RefreshResp, reqwest::Error> {
    let client = reqwest::Client::new();
    let endpoint = format!("{}/{}", server, "authserver/refresh");
    let raw_res = client.post(endpoint).json(&payload).send().await?;
    Ok(raw_res.json().await?)
}

pub async fn refresh_simple(
    server: &str,
    access_token: &str,
    client_token: &str,
) -> Result<RefreshResp, reqwest::Error> {
    let payload = RefreshPayload {
        request_user: Some(true),
        selected_profile: None,
        client_token: client_token.to_string(),
        access_token: access_token.to_string(),
    };
    Ok(refresh(server, payload).await?)
}

pub async fn validate(
    server: &str,
    access_token: &str,
    client_token: Option<&str>,
) -> Result<bool, reqwest::Error> {
    let payload = ValidatePayload {
        client_token: client_token.map(|v| v.to_string()),
        access_token: access_token.to_string(),
    };
    let client = reqwest::Client::new();
    let endpoint = format!("{}/{}", server, "authserver/validate");
    let raw_res = client.post(endpoint).json(&payload).send().await?;
    Ok(raw_res.status().is_success())
}

pub async fn invalidate(
    server: &str,
    access_token: &str,
    client_token: &str,
) -> Result<bool, reqwest::Error> {
    let payload = InvalidatePayload {
        client_token: client_token.to_string(),
        access_token: access_token.to_string(),
    };
    let client = reqwest::Client::new();
    let endpoint = format!("{}/{}", server, "authserver/invalidate");
    let raw_res = client.post(endpoint).json(&payload).send().await?;
    Ok(raw_res.status().is_success())
}

pub async fn signout(server: &str, username: &str, password: &str) -> Result<bool, reqwest::Error> {
    let payload = SignoutPayload {
        username: username.to_string(),
        password: password.to_string(),
    };
    let client = reqwest::Client::new();
    let endpoint = format!("{}/{}", server, "authserver/signout");
    let raw_res = client.post(endpoint).json(&payload).send().await?;
    Ok(raw_res.status().is_success())
}

pub async fn register(
    server: &str,
    username: &str,
    password: &str,
) -> Result<RegisterResp, reqwest::Error> {
    let client = reqwest::Client::new();
    let payload = RegisterPayload {
        username: username.to_string(),
        password: password.to_string(),
    };
    let endpoint = format!("{}/{}", server, "authserver/register");
    let raw_res = client.post(endpoint).json(&payload).send().await?;
    Ok(raw_res.json().await?)
}

pub async fn server_meta(server: &str) -> Result<MetaPayload, reqwest::Error> {
    let client = reqwest::Client::new();
    let raw_res = client.get(server).send().await?;
    Ok(raw_res.json().await?)
}

pub async fn change_password(
    server: &str,
    username: &str,
    old_password: &str,
    new_password: &str,
) -> Result<ChangePassword, reqwest::Error> {
    let client = reqwest::Client::new();
    let payload = ChangePasswordPayload {
        username: username.to_string(),
        old_password: old_password.to_string(),
        new_password: new_password.to_string(),
    };
    let endpoint = format!("{}/{}", server, "authserver/change-password");
    let raw_res = client.post(endpoint).json(&payload).send().await?;
    Ok(raw_res.json().await?)
}
