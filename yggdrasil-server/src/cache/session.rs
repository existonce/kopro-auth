use chrono::{DateTime, Duration, Utc};
use dashmap::DashMap;
use std::{collections::hash_map::RandomState, net::IpAddr, sync::Arc};
use tokio::time::sleep;
use yggdrasil::profile::*;
use yggdrasil::token::*;

pub type SessionCache = Arc<DashMap<AccessToken, Session, RandomState>>;

pub const SESSION_TIMEOUT_SECONDS: i64 = 30;

#[derive(Debug, Clone)]
pub struct Session {
    pub ip: Option<IpAddr>,
    pub server_id: ServerId,
    pub profile: ProfileId,
    pub name: String,
    pub properties: Vec<UserProperty>,
    pub expiring: DateTime<Utc>,
}

impl Session {
    pub fn new(user: &str, server_id: &str, profile: ProfileId, ip: Option<IpAddr>) -> Self {
        let now = chrono::offset::Utc::now();
        Session {
            ip,
            server_id: server_id.to_owned(),
            profile,
            name: user.to_owned(),
            properties: vec![],
            expiring: now + Duration::seconds(SESSION_TIMEOUT_SECONDS),
        }
    }
}
pub fn new_session_cache() -> SessionCache {
    Arc::new(DashMap::new())
}

pub fn run_session_cache_updater(cache: SessionCache) {
    tokio::spawn({
        async move {
            sleep(std::time::Duration::from_secs(
                SESSION_TIMEOUT_SECONDS as u64,
            ))
            .await;
            update_cache(cache);
        }
    });
}

fn update_cache(cache: SessionCache) {
    let mut delvec = vec![];
    let now = chrono::offset::Utc::now();
    for entry in cache.iter() {
        if entry.value().expiring < now {
            delvec.push(entry.key().clone());
        }
    }
    for key in delvec {
        cache.remove(&key);
    }
}
