use std::{
    collections::HashMap,
    fmt::Debug,
    hash::Hash,
    net::IpAddr,
    sync::{Arc, RwLock},
};
use tokio::time::{sleep, Duration};
use tracing::info;

#[derive(Debug)]
pub struct RateLimit;

pub struct Rates {
    pub username_rates: HashMap<String, u32>,
    pub address_rates: HashMap<IpAddr, u32>,
}

pub type RatesCache = Arc<RwLock<Rates>>;

pub const USERNAME_LIMIT: u32 = 100;
pub const ADDRESS_LIMIT: u32 = 100;

impl Rates {
    pub fn new() -> Self {
        Rates {
            username_rates: HashMap::new(),
            address_rates: HashMap::new(),
        }
    }

    pub fn new_cahe() -> RatesCache {
        Arc::new(RwLock::new(Rates::new()))
    }

    pub fn update(&mut self) {
        decrease_values(&mut self.username_rates);
        decrease_values(&mut self.address_rates);
    }

    pub fn try_inc_user(&mut self, username: &str) -> Result<(), RateLimit> {
        try_inc_limit(
            &mut self.username_rates,
            username.to_owned(),
            USERNAME_LIMIT,
        )
    }

    pub fn try_inc_addr(&mut self, address: &IpAddr) -> Result<(), RateLimit> {
        try_inc_limit(&mut self.address_rates, address.to_owned(), ADDRESS_LIMIT)
    }
}

impl Default for Rates {
    fn default() -> Self {
        Self::new()
    }
}

fn decrease_values<T: Eq + Hash + Clone>(m: &mut HashMap<T, u32>) {
    let mut delvec: Vec<T> = vec![];
    for (key, value) in m.iter_mut() {
        if *value > 0 {
            *value -= 1;
        } else {
            delvec.push(key.clone());
        }
    }
    for key in delvec {
        m.remove(&key);
    }
}

fn try_inc_limit<T: Eq + Hash + Debug>(
    m: &mut HashMap<T, u32>,
    key: T,
    limit: u32,
) -> Result<(), RateLimit> {
    info!("Checking rate limit for key: {:?}", key);
    match m.get(&key) {
        Some(&i) => {
            if i >= limit {
                Err(RateLimit)
            } else {
                m.insert(key, i + 1);
                Ok(())
            }
        }
        None => {
            m.insert(key, 1);
            Ok(())
        }
    }
}

pub fn run_rates_cache_updater(cache_ref: RatesCache) {
    tokio::spawn({
        async move {
            loop {
                sleep(Duration::from_secs(60)).await;
                {
                    let mut rates = cache_ref.write().unwrap();
                    rates.update();
                }
            }
        }
    });
}
