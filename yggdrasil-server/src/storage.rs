use super::scheme::*;
use rocksdb::{ColumnFamily, ColumnFamilyDescriptor, Options, WriteBatch, DB};
use serde::{de::DeserializeOwned, Serialize};
use std::{error, fmt, result};
use tracing::info;
use yggdrasil::profile::UserProfile;
use yggdrasil::token::AccessToken;

const USER_FAMILY: &str = "user";
const TOKEN_FAMILY: &str = "token";
const USER_TOKENS_FAMILY: &str = "user_tokens";

#[derive(Debug)]
pub enum Error {
    RocksErr(rocksdb::Error),
    EncoderErr(serde_cbor::Error),
}

/// Alias for a `Result` with the error type `storage::Error`.
pub type Result<T> = result::Result<T, Error>;

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Error::RocksErr(ref e) => write!(f, "Rocksdb error: {}", e),
            Error::EncoderErr(ref e) => write!(f, "Encoding error: {}", e),
        }
    }
}

impl error::Error for Error {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        match self {
            Error::RocksErr(err) => Some(err),
            Error::EncoderErr(err) => Some(err),
        }
    }
}

#[doc(hidden)]
impl From<rocksdb::Error> for Error {
    fn from(error: rocksdb::Error) -> Self {
        Error::RocksErr(error)
    }
}

#[doc(hidden)]
impl From<serde_cbor::error::Error> for Error {
    fn from(error: serde_cbor::error::Error) -> Self {
        Error::EncoderErr(error)
    }
}

pub fn user_family(db: &DB) -> &ColumnFamily {
    db.cf_handle(USER_FAMILY).unwrap()
}

pub fn token_family(db: &DB) -> &ColumnFamily {
    db.cf_handle(TOKEN_FAMILY).unwrap()
}

pub fn user_tokens_family(db: &DB) -> &ColumnFamily {
    db.cf_handle(USER_TOKENS_FAMILY).unwrap()
}

fn column_families() -> Vec<ColumnFamilyDescriptor> {
    vec![
        ColumnFamilyDescriptor::new(USER_FAMILY, Options::default()),
        ColumnFamilyDescriptor::new(TOKEN_FAMILY, Options::default()),
        ColumnFamilyDescriptor::new(USER_TOKENS_FAMILY, Options::default()),
    ]
}

pub fn open_storage(path: &str) -> Result<DB> {
    let mut db_opts = Options::default();
    db_opts.create_missing_column_families(true);
    db_opts.create_if_missing(true);
    let cfs = column_families();
    Ok(DB::open_cf_descriptors(&db_opts, path, cfs)?)
}

pub fn init_storage(path: &str) -> Result<DB> {
    let db = open_storage(path)?;
    init_version(&db)?;
    Ok(db)
}

const VERSION_KEY: &str = "version";

fn init_version(db: &DB) -> Result<()> {
    match db
        .get(VERSION_KEY)?
        .map(|v| serde_cbor::from_slice::<u32>(&v).unwrap())
    {
        None => {
            info!("Initalizing database with version {:?}", SCHEME_VERSION);
            let mut batch = WriteBatch::default();
            batch.put(VERSION_KEY, serde_cbor::to_vec(&SCHEME_VERSION).unwrap());
            db.write(batch)?;
        }
        Some(v) => {
            if v != SCHEME_VERSION {
                // migrations are not implemented yet
                panic!(
                    "Scheme version {} doesn't match current {}",
                    v, SCHEME_VERSION
                );
            }
            info!("Opening database with version {:?}", v);
        }
    }
    Ok(())
}

fn get_record<K: Serialize, T: DeserializeOwned>(
    db: &DB,
    cf: &ColumnFamily,
    k: &K,
) -> Result<Option<T>> {
    let raw_key = serde_cbor::to_vec(&k)?;
    let res = db
        .get_cf(cf, raw_key)?
        .map(|v| serde_cbor::from_slice::<T>(&v));
    Ok(res.transpose()?)
}

fn set_record<K: Serialize, T: Serialize>(
    db: &DB,
    cf: &ColumnFamily,
    k: &K,
    value: &T,
) -> Result<()> {
    let mut batch = WriteBatch::default();
    let raw_key = serde_cbor::to_vec(&k)?;
    let raw_value = serde_cbor::to_vec(value)?;
    batch.put_cf(cf, raw_key, raw_value);
    db.write(batch)?;
    Ok(())
}

pub fn get_user(db: &DB, username: &str) -> Result<Option<UserRecord>> {
    let cf = user_family(db);
    get_record(db, cf, &username)
}

pub fn set_user(db: &DB, user: &UserRecord) -> Result<()> {
    let cf = user_family(db);
    set_record(db, cf, &user.username, user)
}

/// Get associated tokens with user
pub fn get_user_tokens(db: &DB, username: &str) -> Result<UserTokens> {
    let cf = user_tokens_family(db);
    get_record(db, cf, &username).map(|o| o.unwrap_or_default())
}

pub fn set_user_tokens(db: &DB, username: &str, tokens: &UserTokens) -> Result<()> {
    let cf = user_tokens_family(db);
    set_record(db, cf, &username, tokens)
}

pub fn invalidate_all(db: &DB, username: &str) -> Result<()> {
    let mut tokens = get_user_tokens(db, username)?;
    tokens.invalidate_all();
    set_user_tokens(db, username, &tokens)
}

pub fn invalidate_token(db: &DB, username: &str, access_token: &str) -> Result<bool> {
    let mut tokens = get_user_tokens(db, username)?;
    let res = tokens.ivalidate_token(access_token);
    set_user_tokens(db, username, &tokens)?;
    Ok(res)
}

pub fn retreive_token(
    db: &DB,
    username: &str,
    agent: Option<&str>,
    client_token: &str,
    profile: &UserProfile,
) -> Result<AccessToken> {
    let mut tokens = get_user_tokens(db, username)?;
    if let Some(token) = tokens.get_active(client_token) {
        Ok(token)
    } else {
        let record = TokenRecord::new(username, agent, client_token, profile);
        tokens.add_active(&record);
        set_token_record(db, &record)?;
        set_user_tokens(db, username, &tokens)?;
        Ok(record.access_token)
    }
}

pub fn get_token_record(db: &DB, access_token: &str) -> Result<Option<TokenRecord>> {
    let cf = token_family(db);
    get_record(db, cf, &access_token)
}

pub fn set_token_record(db: &DB, record: &TokenRecord) -> Result<()> {
    let cf = token_family(db);
    set_record(db, cf, &record.access_token, record)
}
