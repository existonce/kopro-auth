use crate::cache::rate::*;
use tracing::debug;
use yggdrasil::error::*;

pub trait RejectDb<T> {
    fn reject_db(self) -> Result<T, Error>;
}

impl<T> RejectDb<T> for Result<T, crate::storage::Error> {
    fn reject_db(self) -> Result<T, Error> {
        self.map_err(|e| {
            debug!("Database error: {:?}", e);
            Error::database_error(&format!("{}", e))
        })
    }
}

pub trait RejectRate<T> {
    fn reject_limit(self) -> Result<T, Error>;
}

impl<T> RejectRate<T> for Result<T, RateLimit> {
    fn reject_limit(self) -> Result<T, Error> {
        self.map_err(|_| {
            debug!("Rate limit exceeded");
            Error::rate_limited()
        })
    }
}
