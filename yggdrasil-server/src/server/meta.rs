use warp::reply::json;
use warp::Filter;
use yggdrasil::meta::*;

pub fn meta_routes(
    meta: MetaPayload,
) -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
    warp::get().and(warp::path::end().map(move || json(&meta)))
}
