use super::reject::*;
use crate::cache::session::*;
use crate::storage::{get_token_record, get_user_tokens};
use rocksdb::DB;
use std::net::SocketAddr;
use std::sync::Arc;
use tracing::{debug, error, info};
use warp::path;
use warp::reply::{json, with_status};
use warp::{Filter, Rejection};
use yggdrasil::error::*;
use yggdrasil::join::*;

pub fn session_routes(
    db: Arc<DB>,
) -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
    let cache = new_session_cache();
    run_session_cache_updater(cache.clone());

    let db_local = db;
    let cache_local = cache.clone();
    let join = warp::post().and(
        path!("sessionserver" / "session" / "minecraft" / "join")
            .and(warp::body::json())
            .and(warp::addr::remote())
            .and_then(move |body, client| {
                let db = db_local.clone();
                let cache = cache_local.clone();
                async move {
                    let resp = match join_endpoint(db, body, client, cache).await {
                        Err(body) => with_status(json(&body), warp::http::StatusCode::FORBIDDEN),
                        Ok(()) => with_status(json(&()), warp::http::StatusCode::NO_CONTENT),
                    };
                    debug!("End of join");
                    Result::<_, Rejection>::Ok(resp)
                }
            }),
    );

    let cache_local = cache;
    let has_joined = warp::get().and(
        path!("sessionserver" / "session" / "minecraft" / "hasJoined")
            .and(warp::query::<HasJoinedPayload>())
            .and(warp::addr::remote())
            .and_then(move |body, client| {
                let cache = cache_local.clone();
                async move {
                    let resp = match has_joined_endpoint(body, client, cache).await {
                        Err(body) => with_status(json(&body), warp::http::StatusCode::FORBIDDEN),
                        Ok(body) => with_status(json(&body), warp::http::StatusCode::ACCEPTED),
                    };
                    debug!("End of hasJoined");
                    Result::<_, Rejection>::Ok(resp)
                }
            }),
    );

    join.or(has_joined)
}

async fn join_endpoint(
    db: Arc<DB>,
    body: JoinPayload,
    client_opt: Option<SocketAddr>,
    cache: SessionCache,
) -> Result<(), Error> {
    info!(
        "Join request for token {:?} and server {:?}",
        body.access_token, body.server_id
    );

    let record = match get_token_record(&db, &body.access_token).reject_db()? {
        Some(record) => record,
        None => {
            error!("Not found in database");
            return Err(Error::token_not_valid());
        }
    };
    let tokens = get_user_tokens(&db, &record.username).reject_db()?;
    if tokens.is_active(&record.access_token, &record.client_token) {
        let session = Session::new(
            &record.username,
            &body.server_id,
            record.selected_profile.id,
            client_opt.map(|addr| addr.ip()),
        );
        cache.insert(record.username, session);
        Ok(())
    } else {
        error!("Not active");
        Err(Error::token_not_valid())
    }
}

async fn has_joined_endpoint(
    body: HasJoinedPayload,
    client_opt: Option<SocketAddr>,
    cache: SessionCache,
) -> Result<HasJoined, Error> {
    info!(
        "Has joined request for user {:?} and server {:?}",
        body.username, body.server_id
    );

    let session = match cache.get(&body.username) {
        None => {
            error!(
                "No corresponding join request found for user {:?}",
                body.username
            );
            return Err(Error::token_not_valid());
        }
        Some(session) => session.clone(),
    };

    if session.server_id != body.server_id {
        error!(
            "Server id doesn't match, expected {:?}, but got {:?}",
            session.server_id, body.server_id
        );
        return Err(Error::token_not_valid());
    }
    let client_ip = client_opt.map(|addr| addr.ip());
    if client_ip != session.ip {
        error!(
            "Client id doesn't match, expected {:?}, but got {:?}",
            session.ip, client_ip
        );
        return Err(Error::token_not_valid());
    }

    let resp = HasJoined {
        id: session.profile.clone(),
        name: body.username.clone(),
        properties: Some(session.properties),
    };
    cache.remove(&body.username);
    Ok(resp)
}
