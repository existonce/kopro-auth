pub mod auth;
pub mod meta;
pub mod reject;
pub mod session;

use self::auth::*;
use self::meta::*;
use self::session::*;
use crate::cache::rate::*;
use rocksdb::DB;
use std::net::SocketAddr;
use std::sync::Arc;
use warp::Filter;

pub use yggdrasil::meta::{MetaPayload, ServerLinks};

pub async fn serve_yggdrasil(addr: SocketAddr, db: Arc<DB>, meta: MetaPayload) {
    let rates_cache = Rates::new_cahe();
    run_rates_cache_updater(rates_cache.clone());

    let auth_routes = auth_routes(db.clone(), rates_cache);
    let session_routes = session_routes(db);
    let meta_routes = meta_routes(meta);
    let routes = auth_routes.or(session_routes).or(meta_routes);

    warp::serve(routes).run(addr).await;
}
