use super::reject::*;
use crate::cache::rate::*;
use crate::scheme::*;
use crate::storage::{
    get_token_record, get_user, get_user_tokens, invalidate_all, invalidate_token, retreive_token,
    set_user,
};
use rocksdb::DB;
use std::net::IpAddr;
use std::sync::Arc;
use tracing::{error, info};
use uuid::Uuid;
use warp::reply::{json, with_status};
use warp::{path, Filter, Rejection};
use yggdrasil::auth::*;
use yggdrasil::change::*;
use yggdrasil::error::*;
use yggdrasil::invalidate::*;
use yggdrasil::refresh::*;
use yggdrasil::register::*;
use yggdrasil::signout::*;
use yggdrasil::validate::*;

pub fn auth_routes(
    db: Arc<DB>,
    rates_cache: RatesCache,
) -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
    let db_local = db.clone();
    let rates_local = rates_cache.clone();
    let auth = path!("authserver" / "authenticate")
        .and(warp::body::json())
        .and(warp_real_ip::get_forwarded_for())
        .and_then(move |body, xhdr: Vec<IpAddr>| {
            let db = db_local.clone();
            let rates_cache = rates_local.clone();
            async move {
                let client = xhdr.first();
                let resp = match auth_endpoint(db, rates_cache, body, client.copied()).await {
                    Err(body) => AuthenticateResp::Err(body),
                    Ok(body) => AuthenticateResp::Ok(body),
                };
                Result::<_, Rejection>::Ok(warp::reply::json(&resp))
            }
        })
        .with(warp::trace::named("authenticate"));

    let db_local = db.clone();
    let rates_local = rates_cache.clone();
    let register = path!("authserver" / "register")
        .and(warp::body::json())
        .and(warp_real_ip::get_forwarded_for())
        .and_then(move |body, xhdr: Vec<IpAddr>| {
            let db = db_local.clone();
            let rates_cache = rates_local.clone();
            async move {
                let client = xhdr.first();
                let resp = match register_endpoint(db, rates_cache, body, client.copied()).await {
                    Err(body) => RegisterResp::Err(body),
                    Ok(_) => RegisterResp::Ok(()),
                };
                Result::<_, Rejection>::Ok(json(&resp))
            }
        })
        .with(warp::trace::named("register"));

    let db_local = db.clone();
    let rates_local = rates_cache;
    let change_password = path!("authserver" / "change-password")
        .and(warp::body::json())
        .and(warp_real_ip::get_forwarded_for())
        .and_then(move |body, xhdr: Vec<IpAddr>| {
            let db = db_local.clone();
            let rates_cache = rates_local.clone();
            async move {
                let client = xhdr.first();
                let resp =
                    match change_password_endpoint(db, rates_cache, body, client.copied()).await {
                        Err(body) => ChangePassword::Err(body),
                        Ok(_) => ChangePassword::Ok(()),
                    };
                Result::<_, Rejection>::Ok(json(&resp))
            }
        })
        .with(warp::trace::named("change-password"));

    let db_local = db.clone();
    let validate = path!("authserver" / "validate")
        .and(warp::body::json())
        .and_then(move |body| {
            let db = db_local.clone();
            async move {
                let resp = match validate_endpoint(db, body).await {
                    Err(body) => with_status(json(&body), warp::http::StatusCode::FORBIDDEN),
                    Ok(()) => with_status(json(&()), warp::http::StatusCode::ACCEPTED),
                };
                Result::<_, Rejection>::Ok(resp)
            }
        })
        .with(warp::trace::named("validate"));

    let db_local = db.clone();
    let refresh = path!("authserver" / "refresh")
        .and(warp::body::json())
        .and_then(move |body| {
            let db = db_local.clone();
            async move {
                let resp = match refresh_endpoint(db, body).await {
                    Err(body) => RefreshResp::Err(body),
                    Ok(body) => RefreshResp::Ok(body),
                };
                Result::<_, Rejection>::Ok(warp::reply::json(&resp))
            }
        })
        .with(warp::trace::named("refresh"));

    let db_local = db.clone();
    let invalidate = path!("authserver" / "invalidate")
        .and(warp::body::json())
        .and_then(move |body| {
            let db = db_local.clone();
            async move {
                let resp = match invalidate_endpoint(db, body).await {
                    Err(body) => with_status(json(&body), warp::http::StatusCode::FORBIDDEN),
                    Ok(()) => with_status(json(&()), warp::http::StatusCode::ACCEPTED),
                };
                Result::<_, Rejection>::Ok(resp)
            }
        })
        .with(warp::trace::named("invalidate"));

    let db_local = db;
    let signout = path!("authserver" / "signout")
        .and(warp::body::json())
        .and_then(move |body| {
            let db = db_local.clone();
            async move {
                let resp = match signout_endpoint(db, body).await {
                    Err(body) => with_status(json(&body), warp::http::StatusCode::FORBIDDEN),
                    Ok(()) => with_status(json(&()), warp::http::StatusCode::ACCEPTED),
                };
                Result::<_, Rejection>::Ok(resp)
            }
        })
        .with(warp::trace::named("signout"));

    warp::post().and(
        auth.or(register)
            .or(change_password)
            .or(validate)
            .or(refresh)
            .or(invalidate)
            .or(signout), // .with(warp::trace::request())
    )
}

async fn auth_endpoint(
    db: Arc<DB>,
    rates_cache: RatesCache,
    body: AuthenticatePayload,
    client_opt: Option<IpAddr>,
) -> Result<Authenticate, Error> {
    info!("Auth request for user {:?}", body.username);
    protect_rates(rates_cache, &body.username, client_opt)?;

    match get_user(&db, &body.username).reject_db()? {
        Some(user) => {
            if user.check_password(&body.password) {
                let client_token = if let Some(token) = body.client_token {
                    token
                } else {
                    info!("Invalidating old tokens for {:?}", body.username);
                    invalidate_all(&db, &body.username).reject_db()?;
                    Uuid::new_v4().to_string()
                };
                let agent_name = body.agent.as_ref().map(|a| a.name.clone());

                let selected_profile = if let Some(agent) = &body.agent {
                    user.profiles
                        .iter()
                        .find(|p| p.agent.as_ref() == Some(&agent.name))
                } else {
                    None
                }
                .unwrap_or_else(|| user.profiles.first().unwrap());

                let access_token = retreive_token(
                    &db,
                    &body.username,
                    agent_name.as_deref(),
                    &client_token,
                    &selected_profile,
                )
                .reject_db()?;
                let user_meta = body.request_user.map(|_| user.meta.clone());

                info!("Succeded auth for {:?}", body.username);
                Ok(Authenticate {
                    user: user_meta,
                    selected_profile: Some(selected_profile.clone()),
                    available_profiles: Some(user.profiles.clone()),
                    client_token,
                    access_token,
                })
            } else {
                error!("Failed auth for {:?}", body.username);
                Err(Error::invalid_credentials())
            }
        }
        None => {
            error!("Failed auth for {:?}", body.username);
            Err(Error::missing_user())
        }
    }
}

async fn register_endpoint(
    db: Arc<DB>,
    rates_cache: RatesCache,
    body: RegisterPayload,
    client_opt: Option<IpAddr>,
) -> Result<(), Error> {
    info!("Register request for user {:?}", body.username);
    protect_rates(rates_cache, &body.username, client_opt)?;

    match get_user(&db, &body.username).reject_db()? {
        Some(_) => {
            error!("User {:?} already registered", body.username);
            Err(Error::user_registered())
        }
        None => {
            info!("New user {:?} registered", body.username);
            let user = UserRecord::register(&body.username, &body.password);
            set_user(&db, &user).reject_db()?;
            Ok(())
        }
    }
}

async fn change_password_endpoint(
    db: Arc<DB>,
    rates_cache: RatesCache,
    body: ChangePasswordPayload,
    client_opt: Option<IpAddr>,
) -> Result<(), Error> {
    info!("Change password request for user {:?}", body.username);
    protect_rates(rates_cache, &body.username, client_opt)?;

    match get_user(&db, &body.username).reject_db()? {
        Some(mut user) => {
            if user.check_password(&body.old_password) {
                user.change_password(&body.new_password);
                set_user(&db, &user).reject_db()?;
                Ok(())
            } else {
                error!("Failed auth for {:?}", body.username);
                Err(Error::invalid_credentials())
            }
        }
        None => {
            error!("No user {:?} known", body.username);
            Err(Error::missing_user())
        }
    }
}

fn protect_rates(
    rates_cache: RatesCache,
    username: &str,
    client_opt: Option<IpAddr>,
) -> Result<(), Error> {
    let mut rates = rates_cache.write().unwrap();
    if let Some(client) = client_opt {
        rates.try_inc_addr(&client).reject_limit()?
    }
    rates.try_inc_user(username).reject_limit()
}

async fn validate_endpoint(db: Arc<DB>, body: ValidatePayload) -> Result<(), Error> {
    info!("Validate request for token {:?}", body.access_token);

    let record = match get_token_record(&db, &body.access_token).reject_db()? {
        Some(record) => record,
        None => {
            error!("Not found in database");
            return Err(Error::token_not_valid());
        }
    };
    match body.client_token {
        Some(t) if t != record.client_token => {
            error!("Not matched client token");
            return Err(Error::token_not_valid());
        }
        _ => (),
    }
    let tokens = get_user_tokens(&db, &record.username).reject_db()?;
    if tokens.is_active(&record.access_token, &record.client_token) {
        info!("Valid token {:?}", body.access_token);
        Ok(())
    } else {
        error!("Not active");
        Err(Error::token_not_valid())
    }
}

async fn refresh_endpoint(db: Arc<DB>, body: RefreshPayload) -> Result<Refresh, Error> {
    info!("Refresh request for token {:?}", body.access_token);

    let record = match get_token_record(&db, &body.access_token).reject_db()? {
        Some(record) => record,
        None => {
            error!("Not found in database");
            return Err(Error::token_not_valid());
        }
    };
    if body.client_token != record.client_token {
        error!("Not matched client token");
        return Err(Error::token_not_valid());
    }
    let was_active = invalidate_token(&db, &record.username, &body.access_token).reject_db()?;
    if !was_active {
        error!("Token was active");
        return Err(Error::token_not_valid());
    }

    let user = match get_user(&db, &record.username).reject_db()? {
        Some(user) => user,
        None => {
            error!("Cannot find user {:?} in database", record.username);
            return Err(Error::token_not_valid());
        }
    };

    let user_meta = body.request_user.map(|_| user.meta.clone());
    let selected_profile = if let Some(agent) = &record.agent {
        user.profiles
            .iter()
            .find(|p| p.agent.as_ref() == Some(agent))
    } else {
        None
    }
    .unwrap_or_else(|| user.profiles.first().unwrap());

    let access_token = retreive_token(
        &db,
        &record.username,
        record.agent.as_deref(),
        &record.client_token,
        &selected_profile,
    )
    .reject_db()?;

    info!(
        "Refreshed token {:?} for user {:?}",
        access_token, record.username
    );
    Ok(Refresh {
        user: user_meta,
        selected_profile: Some(selected_profile.clone()),
        client_token: record.client_token,
        access_token,
    })
}

async fn invalidate_endpoint(db: Arc<DB>, body: InvalidatePayload) -> Result<(), Error> {
    info!(
        "Invalidate request for token {:?} and client {:?}",
        body.access_token, body.client_token
    );

    let record = match get_token_record(&db, &body.access_token).reject_db()? {
        Some(record) => record,
        None => {
            error!("Not found in database");
            return Err(Error::token_not_valid());
        }
    };
    if body.client_token != record.client_token {
        error!("Not matched client token");
        return Err(Error::token_not_valid());
    }
    let was_active = invalidate_token(&db, &record.username, &body.access_token).reject_db()?;
    if !was_active {
        error!("Token was active");
        return Err(Error::token_not_valid());
    }
    Ok(())
}

async fn signout_endpoint(db: Arc<DB>, body: SignoutPayload) -> Result<(), Error> {
    info!("Signout request for user {:?}", body.username);

    match get_user(&db, &body.username).reject_db()? {
        Some(user) => {
            if user.check_password(&body.password) {
                invalidate_all(&db, &body.username).reject_db()?;
                Ok(())
            } else {
                error!("Failed auth for {:?}", body.username);
                Err(Error::invalid_credentials())
            }
        }
        None => {
            error!("Failed signout {:?}", body.username);
            Err(Error::invalid_credentials())
        }
    }
}
