extern crate chrono;
extern crate clap;
extern crate pbkdf2;
extern crate serde;
extern crate serde_cbor;
extern crate tokio;
extern crate uuid;
extern crate yggdrasil_client;

pub mod cache;
pub mod scheme;
pub mod server;
pub mod storage;

use clap::Parser;
use server::*;
use std::error::Error;
use std::fs::read_to_string;
use std::net::{IpAddr, SocketAddr};
use std::sync::Arc;
use storage::init_storage;
use tracing::info;
use tracing_subscriber::fmt::format::FmtSpan;
use yggdrasil::meta::*;

#[derive(Parser, Debug)]
#[clap(version, author = "schoolgirl <existonce@protonmail.com>")]
struct Opts {
    #[clap(short, long, default_value = "0.0.0.0")]
    host: IpAddr,
    #[clap(short, long, default_value = "3001")]
    port: u16,
    #[clap(short, long, default_value = "./yggdrasil_db")]
    data: String,
    #[clap(long)]
    /// Path to file with PEM encoded GPG public key
    public_key: Option<String>,
    #[clap(long)]
    /// Server nickname displayed in root endpoint
    server_name: Option<String>,
    #[clap(long)]
    /// Server homepage URL
    homepage_url: Option<String>,
    #[clap(long)]
    /// Server register URL
    register_url: Option<String>,
    #[clap(long)]
    /// Launcher URL with placeholder {} for platform tag (winows, linux, mac). Example: https://example.com/launcher-{}.zip
    launcher_url: Option<String>,
    #[clap(long)]
    /// Client URL with placeholder {} for platform tag (winows, linux, mac). Example: https://example.com/client-{}.zip
    client_url: Option<String>,
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    let opts: Opts = Opts::parse();

    let filter = std::env::var("RUST_LOG")
        .unwrap_or_else(|_| "yggdrasil_server=info,tracing=info,warp=debug".to_owned());
    tracing_subscriber::fmt()
        .with_env_filter(filter)
        .with_span_events(FmtSpan::CLOSE)
        .init();

    info!("Opening database {:?}", opts.data);
    let db = Arc::new(init_storage(&opts.data)?);

    let addr = SocketAddr::new(opts.host, opts.port);
    let meta = make_meta(&opts);
    serve_yggdrasil(addr, db.clone(), meta).await;
    Ok(())
}

fn make_meta(opts: &Opts) -> MetaPayload {
    let mut meta = MetaPayload::default();
    if let Some(path) = opts.public_key.as_ref() {
        let key = read_to_string(path).unwrap();
        meta.signature_publickey = key;
    }
    if let Some(name) = opts.server_name.as_ref() {
        meta.meta.server_name = Some(name.clone());
    }
    if let Some(url) = opts.homepage_url.as_ref() {
        let mut links = meta
            .meta
            .links
            .as_ref()
            .map_or_else(ServerLinks::new, |links| links.clone());
        links.homepage = Some(url.clone());
        meta.meta.links = Some(links);
    }
    if let Some(url) = opts.register_url.as_ref() {
        let mut links = meta
            .meta
            .links
            .as_ref()
            .map_or_else(ServerLinks::new, |links| links.clone());
        links.register = Some(url.clone());
        meta.meta.links = Some(links);
    }
    if let Some(url) = opts.launcher_url.as_ref() {
        let mut links = meta
            .meta
            .links
            .as_ref()
            .map_or_else(ServerLinks::new, |links| links.clone());
        links.launcher = Some(PlatformLinks::from_template(&url.clone()));
        meta.meta.links = Some(links);
    }
    if let Some(url) = opts.client_url.as_ref() {
        let mut links = meta
            .meta
            .links
            .as_ref()
            .map_or_else(ServerLinks::new, |links| links.clone());
        links.client = Some(PlatformLinks::from_template(&url.clone()));
        meta.meta.links = Some(links);
    }
    meta
}
