use chrono::{DateTime, Utc};
use pbkdf2::{
    password_hash::{PasswordHash, PasswordHasher, PasswordVerifier, SaltString},
    Pbkdf2,
};
use rand_core::OsRng;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use uuid::Uuid;
use yggdrasil::profile::{User, UserProfile};
use yggdrasil::token::{AccessToken, ClientToken};

pub const SCHEME_VERSION: u32 = 1;

#[derive(Deserialize, Serialize, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct UserRecord {
    pub username: String,
    pub password_hash: String,
    pub created_at: DateTime<Utc>,
    pub meta: User,
    pub profiles: Vec<UserProfile>,
}

impl UserRecord {
    pub fn register(username: &str, password: &str) -> Self {
        let now = chrono::offset::Utc::now();
        let now_millis = now.timestamp_millis() as u64;
        UserRecord {
            username: username.to_owned(),
            password_hash: hash_password(password),
            created_at: now,
            meta: User {
                id: Uuid::new_v4().to_string(),
                email: None,
                username: Some(username.to_owned()),
                register_ip: None,
                migrated_from: None,
                migrated_at: None,
                registered_at: Some(now_millis),
                password_changed_at: None,
                date_of_birth: None,
                suspended: None,
                blocked: None,
                secured: None,
                migrated: None,
                email_verified: None,
                legacy_user: Some(true),
                verified_by_parent: None,
                properties: None,
            },
            profiles: vec![UserProfile {
                agent: Some("Minecraft".to_owned()),
                id: Uuid::new_v4().to_string(),
                name: username.to_owned(),
                user_id: None,
                created_at: Some(now_millis),
                legacy_profile: None,
                suspended: None,
                paid: None,
                migrated: None,
                legacy: Some(true),
            }],
        }
    }

    pub fn check_password(&self, password: &str) -> bool {
        let parsed_hash = PasswordHash::new(&self.password_hash).unwrap();
        Pbkdf2
            .verify_password(password.as_bytes(), &parsed_hash)
            .is_ok()
    }

    pub fn change_password(&mut self, password: &str) -> &mut UserRecord {
        self.password_hash = hash_password(password);
        self
    }
}

fn hash_password(password: &str) -> String {
    let salt = SaltString::generate(&mut OsRng);
    Pbkdf2
        .hash_password_simple(password.as_bytes(), salt.as_ref())
        .unwrap()
        .to_string()
}

#[derive(Deserialize, Serialize, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct TokenRecord {
    pub username: String,
    pub agent: Option<String>,
    pub access_token: AccessToken,
    pub client_token: ClientToken,
    pub created_at: DateTime<Utc>,
    pub valid_till: Option<DateTime<Utc>>,
    pub invalidated_at: Option<DateTime<Utc>>,
    pub selected_profile: UserProfile,
}

impl TokenRecord {
    pub fn new(
        username: &str,
        agent: Option<&str>,
        client_token: &str,
        profile: &UserProfile,
    ) -> Self {
        TokenRecord {
            username: username.to_owned(),
            agent: agent.map(|a| a.to_owned()),
            access_token: Uuid::new_v4().to_string(),
            client_token: client_token.to_owned(),
            created_at: chrono::offset::Utc::now(),
            valid_till: None,
            invalidated_at: None,
            selected_profile: profile.clone(),
        }
    }

    pub fn invalidate(&mut self) {
        self.invalidated_at = Some(chrono::offset::Utc::now());
    }
}

#[derive(Deserialize, Serialize, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct TokenMeta {
    pub access_token: AccessToken,
    pub valid_till: Option<DateTime<Utc>>,
}

#[derive(Deserialize, Serialize, Debug, PartialEq, Eq)]
pub struct UserTokens {
    pub active_tokens: HashMap<ClientToken, TokenMeta>,
    pub invalidated_tokens: Vec<AccessToken>,
}

impl UserTokens {
    pub fn new() -> Self {
        UserTokens::default()
    }

    pub fn invalidate_all(&mut self) {
        for token in self.active_tokens.values() {
            self.invalidated_tokens.push(token.access_token.clone());
        }
        self.active_tokens.clear();
    }

    pub fn ivalidate_token(&mut self, access_token: &str) -> bool {
        let mut del_key = None;
        for (key, token) in self.active_tokens.iter() {
            if token.access_token == access_token {
                del_key = Some(key.clone());
                self.invalidated_tokens.push(token.access_token.clone());
            }
        }
        let res = del_key.is_some();
        del_key.map(|k| self.active_tokens.remove(&k));
        res
    }

    pub fn get_active(&self, client_token: &str) -> Option<AccessToken> {
        self.active_tokens.get(client_token).and_then(|meta| {
            let active = meta
                .valid_till
                .map_or(true, |t| chrono::offset::Utc::now() < t);
            if active {
                Some(meta.access_token.clone())
            } else {
                None
            }
        })
    }

    pub fn add_active(&mut self, token_record: &TokenRecord) {
        self.active_tokens.insert(
            token_record.client_token.clone(),
            TokenMeta {
                access_token: token_record.access_token.clone(),
                valid_till: token_record.valid_till,
            },
        );
    }

    pub fn is_active(&self, access_token: &str, client_token: &str) -> bool {
        self.get_active(client_token)
            .map(|t| t == access_token)
            .unwrap_or(false)
    }
}

impl Default for UserTokens {
    fn default() -> Self {
        UserTokens {
            active_tokens: HashMap::new(),
            invalidated_tokens: Vec::new(),
        }
    }
}
