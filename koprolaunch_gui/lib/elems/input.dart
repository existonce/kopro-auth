import 'package:flutter/material.dart';

class LabeledInput extends StatelessWidget {
  LabeledInput({required this.label, this.obscureText});

  final String label;
  final bool? obscureText;

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      Container(
        alignment: Alignment.topLeft,
        child: Text(
          label,
          style: Theme.of(context).inputDecorationTheme.labelStyle,
        ),
      ),
      Padding(
        padding: EdgeInsets.only(top: 5),
        child: TextFormField(
          obscureText: obscureText ?? false,
          decoration: InputDecoration(
            border: OutlineInputBorder(),
            isDense: true,
          ),
          validator: (value) {
            if (value == null || value.isEmpty) {
              return label + ' is required';
            }
            return null;
          },
        ),
      ),
    ]);
  }
}
