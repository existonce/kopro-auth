import 'package:flutter/material.dart';
import '/theme.dart';

class PrimaryButton extends StatelessWidget {
  PrimaryButton({required this.formKey, required this.title});

  final GlobalKey<FormState> formKey;
  final String title;

  @override
  Widget build(BuildContext context) {
    return DecoratedBox(
      decoration: BoxDecoration(
        gradient: LinearGradient(colors: [
          Theme.of(context).primaryButtonColor,
          Theme.of(context).brightButtonColor,
          Theme.of(context).primaryButtonColor,
        ], begin: Alignment.topCenter, end: Alignment.bottomCenter),
        borderRadius: BorderRadius.all(Radius.circular(5)),
      ),
      child: MaterialButton(
        height: 45,
        color: Colors.transparent,
        splashColor: Colors.orangeAccent,
        onPressed: () {
          if (formKey.currentState!.validate()) {
            ScaffoldMessenger.of(context)
                .showSnackBar(SnackBar(content: Text('Processing Data')));
          }
        },
        child: Container(
          height: 45,
          alignment: Alignment.center,
          child: Text(
            title,
            style: TextStyle(fontSize: 18),
          ),
        ),
      ),
    );
  }
}

class SecondaryButton extends StatelessWidget {
  SecondaryButton({required this.formKey, required this.title});

  final GlobalKey<FormState> formKey;
  final String title;

  @override
  Widget build(BuildContext context) {
    return DecoratedBox(
      decoration: BoxDecoration(
        border: Border.all(
          color: Theme.of(context).buttonBorderColor,
          width: 1,
        ),
        borderRadius: BorderRadius.all(Radius.circular(5)),
      ),
      child: MaterialButton(
        height: 45,
        color: Theme.of(context).primaryColor.withAlpha(179),
        splashColor: Colors.orangeAccent,
        onPressed: () {
          if (formKey.currentState!.validate()) {
            ScaffoldMessenger.of(context)
                .showSnackBar(SnackBar(content: Text('Processing Data')));
          }
        },
        child: Container(
          height: 45,
          alignment: Alignment.center,
          child: Text(
            title,
            style: TextStyle(fontSize: 18),
          ),
        ),
      ),
    );
  }
}
