import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';

import 'theme.dart';
import 'screens.dart';

void main() {
  runApp(KoprolaunchApp());
}

class KoprolaunchApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Koprolaunch',
      theme: appTheme(),
      initialRoute: '/',
      onGenerateRoute: (settings) {
        switch (settings.name) {
          case '/':
            return PageTransition(
              child: LoginScreen(title: "Koprokubach"),
              type: PageTransitionType.fade,
              settings: settings,
            );
          case '/register':
            return PageTransition(
              child: RegisterScreen(title: "Registration"),
              type: PageTransitionType.fade,
              settings: settings,
            );
          case '/settings':
            return PageTransition(
              child: RegisterScreen(title: "Settings"),
              type: PageTransitionType.fade,
              settings: settings,
            );
          case '/changepass':
            return PageTransition(
              child: ChangePasswordScreen(title: "Change password"),
              type: PageTransitionType.fade,
              settings: settings,
            );
          default:
            return null;
        }
      },
    );
  }
}
