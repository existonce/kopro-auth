import 'package:flutter/material.dart';

extension LauncherTextTheme on TextTheme {
  TextStyle get disabledHeadline1 => TextStyle(
      fontSize: 24.0, color: Color(0xFF939393), fontWeight: FontWeight.normal);
}

extension LauncherTheme on ThemeData {
  Color get primaryButtonColor => Color(0xFF004E9B);
  Color get brightButtonColor => Color(0xFF3f7ab3);
  Color get buttonBorderColor => Colors.white;
}

ThemeData appTheme() {
  return ThemeData(
    brightness: Brightness.dark,
    primaryColor: Color(0xFF30475E),
    disabledColor: Color(0xFF939393),
    fontFamily: 'Roboto',
    textTheme: TextTheme(
      headline1: TextStyle(
          fontSize: 24.0, color: Colors.white, fontWeight: FontWeight.normal),
    ),
    inputDecorationTheme: InputDecorationTheme(
      filled: true,
      fillColor: Color(0xFF222831).withAlpha(128),
      focusedBorder:
          OutlineInputBorder(borderSide: BorderSide(color: Color(0xFFF2A365))),
      labelStyle: TextStyle(fontSize: 18.0, color: Color(0xFF939393)),
      errorStyle: TextStyle(fontSize: 15.0, fontWeight: FontWeight.bold),
    ),
    errorColor: Colors.red,
    snackBarTheme: SnackBarThemeData(
      backgroundColor: Color(0xFF30475E),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
      contentTextStyle: TextStyle(fontSize: 18, color: Colors.white),
      behavior: SnackBarBehavior.floating,
    ),
  );
}
