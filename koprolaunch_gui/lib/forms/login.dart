import 'package:flutter/material.dart';
import '/elems/button.dart';
import '/elems/input.dart';

class LoginForm extends StatefulWidget {
  @override
  LoginFormState createState() {
    return LoginFormState();
  }
}

class LoginFormState extends State<LoginForm> {
  // Create a global key that uniquely identifies the Form widget
  // and allows validation of the form.
  //
  // Note: This is a `GlobalKey<FormState>`,
  // not a GlobalKey<MyCustomFormState>.
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Center(
        child: Container(
          width: 190,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(top: 100),
                child: LabeledInput(
                  label: "Nickname",
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 20),
                child: LabeledInput(
                  label: "Password",
                  obscureText: true,
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 20),
                child: PrimaryButton(title: 'Connect', formKey: _formKey),
              ),
              Padding(
                padding: EdgeInsets.only(top: 20),
                child: SecondaryButton(title: 'Offline', formKey: _formKey),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
