import 'package:flutter/material.dart';
import 'theme.dart';

enum MenuItem {
  play,
  register,
  settings,
  changePassword,
}

extension MenuExtension on MenuItem {
  String get buttonLabel {
    switch (this) {
      case MenuItem.play:
        return "PLAY";
      case MenuItem.register:
        return "REGISTER";
      case MenuItem.settings:
        return "SETTINGS";
      case MenuItem.changePassword:
        return "CHANGE PASS";
    }
  }

  String get path {
    switch (this) {
      case MenuItem.play:
        return "/";
      case MenuItem.register:
        return "/register";
      case MenuItem.settings:
        return "/settings";
      case MenuItem.changePassword:
        return "/changepass";
    }
  }
}

class Menu extends StatefulWidget {
  const Menu({Key? key, this.item}) : super(key: key);

  final MenuItem? item;

  @override
  _MenuState createState() => _MenuState();
}

class _MenuState extends State<Menu> {
  MenuItem? hoveredItem;

  _MenuState() : super();

  void _setHovered(MenuItem? item) {
    setState(() {
      hoveredItem = item;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        menuRow(context, MenuItem.play),
        menuRow(context, MenuItem.register),
        menuRow(context, MenuItem.settings),
        menuRow(context, MenuItem.changePassword),
      ],
    );
  }

  TextStyle? itemStyle(BuildContext context, MenuItem curItem) {
    if (curItem == widget.item)
      return Theme.of(context).textTheme.headline1;
    else if (curItem == hoveredItem)
      return Theme.of(context).textTheme.headline1!.copyWith(
        shadows: [
          Shadow(
            offset: Offset(1.0, 1.0),
            blurRadius: 3.0,
            color: Color.fromARGB(255, 255, 255, 255),
          ),
        ],
      );
    else
      return Theme.of(context).textTheme.disabledHeadline1;
  }

  Widget menuRow(BuildContext context, MenuItem curItem) {
    return GestureDetector(
        onTap: () {
          Navigator.pushNamed(context, curItem.path);
        },
        child: MouseRegion(
            onEnter: (event) => _setHovered(curItem),
            onExit: (event) => _setHovered(null),
            child: Container(
                margin: EdgeInsets.only(left: 20, top: 20),
                child: Text(curItem.buttonLabel,
                    style: itemStyle(context, curItem)))));
  }
}
