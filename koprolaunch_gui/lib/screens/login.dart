import 'package:flutter/material.dart';
import 'template.dart';
import '../forms/login.dart';

class LoginScreen extends StatefulWidget {
  LoginScreen({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  @override
  Widget build(BuildContext context) {
    return BaseScreen(
      title: widget.title,
      child: Column(
        children: [LoginForm()],
      ),
      menuItem: MenuItem.play,
    );
  }
}
