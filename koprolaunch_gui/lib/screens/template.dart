import 'package:flutter/material.dart';
import '/menu.dart';
export '/menu.dart';

class BaseScreen extends StatelessWidget {
  BaseScreen(
      {Key? key,
      required this.title,
      required this.child,
      required this.menuItem})
      : super(key: key);

  final String title;
  final Widget child;
  final MenuItem menuItem;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/images/background.png"),
                fit: BoxFit.cover,
              ),
            ),
          ),
          Scaffold(
            backgroundColor: Colors.transparent,
            appBar: AppBar(
              title: Center(
                child: Text(
                  this.title,
                  style: Theme.of(context).textTheme.headline1,
                ),
              ),
              backgroundColor: Theme.of(context).primaryColor.withAlpha(179),
              elevation: 0.0,
            ),
            body: Center(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    width: 210,
                    color: Theme.of(context).primaryColor.withAlpha(128),
                    child: Menu(item: this.menuItem),
                  ),
                  Expanded(child: this.child),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
