import 'package:flutter/material.dart';
import 'template.dart';

class RegisterScreen extends StatefulWidget {
  RegisterScreen({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  @override
  Widget build(BuildContext context) {
    return BaseScreen(
      title: widget.title,
      child: Row(),
      menuItem: MenuItem.register,
    );
  }
}
