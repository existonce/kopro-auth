import 'package:flutter/material.dart';
import 'template.dart';

class ChangePasswordScreen extends StatefulWidget {
  ChangePasswordScreen({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  _ChangePasswordScreenState createState() => _ChangePasswordScreenState();
}

class _ChangePasswordScreenState extends State<ChangePasswordScreen> {
  @override
  Widget build(BuildContext context) {
    return BaseScreen(
      title: widget.title,
      child: Row(),
      menuItem: MenuItem.changePassword,
    );
  }
}
