import 'package:flutter/material.dart';
import 'template.dart';

class SettingsScreen extends StatefulWidget {
  SettingsScreen({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  @override
  Widget build(BuildContext context) {
    return BaseScreen(
      title: widget.title,
      child: Row(),
      menuItem: MenuItem.settings,
    );
  }
}
