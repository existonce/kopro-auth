{ nixpkgs ? fetchTarball "https://github.com/NixOS/nixpkgs/archive/6043f15c17fddf22ee840c205e25f481585cdea2.tar.gz"
, pkgs ? (import nixpkgs {}).pkgsCross.aarch64-darwin
}:

# # callPackage is needed due to https://github.com/NixOS/nixpkgs/pull/126844
# pkgs.pkgsStatic.callPackage ({ mkShell, zlib, pkg-config, file }: mkShell {
#   # these tools run on the build platform, but are configured to target the host platform
#   nativeBuildInputs = [ pkg-config file ];
#   # libraries needed for the host platform
#   buildInputs = [ zlib ];
# }) {}
with pkgs;
mkShell {
  buildInputs = [ zlib ]; # your dependencies here
}