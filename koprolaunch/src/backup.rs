use super::error::*;
use fs_extra::dir::CopyOptions;
use std::path::{Path, PathBuf};
use log::*;

pub fn backup_client<P: AsRef<Path>>(state_dir: P) -> Result<()> {
    if state_dir.as_ref().exists() {
        let state_name = state_dir.as_ref().file_name().unwrap();
        let backup_path = find_backup_path(&state_dir)?;
        let backup_name = &backup_path.file_name().unwrap();
        if backup_path.exists() {
            info!("Removing old backup at {:?}", backup_path);
            std::fs::remove_dir_all(&backup_path)?;
        }
        info!("Making backup of {:?}", state_dir.as_ref());
        {
            let tmp_dir = tempfile::tempdir()?;
            let options = CopyOptions::new();
            fs_extra::dir::copy(&state_dir, &tmp_dir, &options)?;
            let mut tmp_from = tmp_dir.path().to_path_buf();
            tmp_from.push(&state_name);
            let mut tmp_to = tmp_dir.path().to_path_buf();
            tmp_to.push(&backup_name);
            std::fs::rename(&tmp_from, &tmp_to)?;
            fs_extra::dir::move_dir(tmp_to, state_dir.as_ref().parent().unwrap(), &options)?;
        }
    }
    Ok(())
}

fn find_backup_path<P: AsRef<Path>>(state_dir: P) -> Result<PathBuf> {
    let state_name = state_dir.as_ref().file_name().unwrap();
    let parent_path = state_dir.as_ref().parent().unwrap();
    let mut i = 0;
    loop {
        let mut path = parent_path.to_path_buf();
        let backup_name = format!("{}_backup_{}", state_name.to_str().unwrap(), i);
        path.push(backup_name);
        if path.exists() {
            i += 1;
        } else {
            return Ok(path);
        }
    }
}
