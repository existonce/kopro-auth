use super::error::*;
use crate::info::{info_from_dir, write_info_dir, LastLogin};
use std::io::{self, Write};
use std::path::Path;
use std::process::{Child, Command};
use text_io::scan;
use yggdrasil::{
    auth::AuthenticateResp, meta::MetaPayload, refresh::RefreshResp, register::RegisterResp,
    token::new_client_token,
};
use yggdrasil_client::endpoints::{authenticate_simple, refresh_simple, register, signout};
use log::*;

pub const OFFLINE_USERNAME: &str = "schoolgirl";

pub struct LaunchArgs {
    pub min_memory: String,
    pub max_memory: String,
    pub additional_args: String,
    pub java_path: Option<String>,
    pub use_nix: bool,
}

pub async fn signout_client<P: AsRef<Path>>(
    server: &str,
    state_dir: P,
    offline: bool,
) -> Result<()> {
    let mut info = match info_from_dir(&state_dir)? {
        None => {
            return Err(Error::ExpectedVersionInfo(
                state_dir.as_ref().to_str().unwrap().to_owned(),
            ))
        }
        Some(v) => v,
    };
    match info.last_login {
        None => info!("Not signed in"),
        Some(pwbox) => {
            if offline {
                info!("Forgeting only local token. Other sessions are not disabled, use without --offline flag to signout all tokens");
                info.last_login = None;
                write_info_dir(&state_dir, &info)?;
            } else {
                info!("Require password to disable all tokens on server. Use --offline to forget only local token");
                let password = rpassword::read_password_from_tty(Some("Password: "))?;
                let last_login = LastLogin::unseal(&pwbox, &password)?;
                let res = signout(server, &last_login.selected_profile.name, &password).await?;
                if res {
                    info!("Signed out");
                    info.last_login = None;
                    write_info_dir(&state_dir, &info)?;
                } else {
                    error!("Failed to sign out, try again");
                }
            }
        }
    }
    Ok(())
}

pub async fn launch_client<P: AsRef<Path>>(
    server: &str,
    state_dir: P,
    meta: &MetaPayload,
    muser: Option<&str>,
    mpassword: Option<&str>,
    launch_args: &LaunchArgs,
    offline: bool,
) -> Result<Child> {
    let last_login = get_last_login(server, &state_dir, muser, mpassword, offline).await?;
    let prefetch = meta.base64().unwrap();
    let nix_shell = is_nixos() || launch_args.use_nix;
    let mut cmd = if cfg!(target_os = "windows") {
        Command::new("cmd")
    } else if nix_shell {
        Command::new("nix-shell")
    } else {
        Command::new("sh")
    };
    cmd.current_dir(&state_dir);
    if cfg!(target_os = "windows") {
        cmd.args(&["/C", "start.bat"]);
    } else if nix_shell {
        cmd.arg("--command").arg("./start.sh");
    } else {
        cmd.arg("-c").arg("./start.sh");
    }

    let proccess = cmd
        .env("PREFETCH", &prefetch)
        .env("USERNAME", &last_login.selected_profile.name)
        .env("ACCESS_TOKEN", &last_login.access_token)
        .env("PROFILE_UUID", &last_login.selected_profile.id)
        .env("MIN_MEMORY", &launch_args.min_memory)
        .env("MAX_MEMORY", &launch_args.max_memory)
        .env("MINECRAFT_ARGS", &launch_args.additional_args)
        .env("JAVAPATH", launch_args.java_path.as_deref().unwrap_or("java"))
        .spawn()?;
    Ok(proccess)
}

fn is_nixos() -> bool {
    which::which("nixos-rebuild").is_ok()
}

pub fn ask_username() -> String {
    print!("Username: ");
    io::stdout().flush().unwrap();
    let username: String;
    if cfg!(target_os = "windows") {
        scan!("{}\r\n", username);
    } else {
        scan!("{}\n", username);
    }
    username
}

async fn get_last_login<P: AsRef<Path>>(
    server: &str,
    state_dir: P,
    muser: Option<&str>,
    mpassword: Option<&str>,
    offline: bool,
) -> Result<LastLogin> {
    let mut info = match info_from_dir(&state_dir)? {
        None => {
            return Err(Error::ExpectedVersionInfo(
                state_dir.as_ref().to_str().unwrap().to_owned(),
            ))
        }
        Some(v) => v,
    };

    let last_login = if offline {
        info!("Using stub profile for offline startup");
        let user: String = match muser {
            None => OFFLINE_USERNAME.to_owned(),
            Some(user) => user.into(),
        };
        LastLogin::offline(&user)
    } else {
        match info.last_login {
            None => {
                info!("No previous login info");
                let user: String = match muser {
                    None => ask_username(),
                    Some(user) => user.into(),
                };
                let password: String = match mpassword {
                    None => rpassword::read_password_from_tty(Some("Password: "))?,
                    Some(pass) => pass.into(),
                };
                let last_login = match signin_user(server, &user, &password).await {
                    Ok(v) => v,
                    Err(Error::YggdrasilError(e))
                        if e.error == yggdrasil::error::Error::missing_user().error =>
                    {
                        info!("User is not registered, trying to register it. Repeat your password to double check.");
                        let second_password: String = match mpassword {
                            None => rpassword::read_password_from_tty(Some("Password: "))?,
                            Some(pass) => pass.into(),
                        };
                        if password != second_password {
                            return Err(Error::PasswordsAreNotMatched);
                        }
                        if let RegisterResp::Ok(_) = register(server, &user, &password).await? {
                            info!("Registered new user {}", user)
                        }
                        signin_user(server, &user, &password).await?
                    }
                    Err(e) => return Err(e),
                };
                let pwbox = last_login.seal(&password)?;
                info.last_login = Some(pwbox);
                write_info_dir(&state_dir, &info)?;
                last_login
            }
            Some(pwbox) => {
                info!("Found encrypted token from previous login. Require password to decrypt.");
                let password: String = match mpassword {
                    None => rpassword::read_password_from_tty(Some("Password: "))?,
                    Some(pass) => pass.into(),
                };
                let last_login = LastLogin::unseal(&pwbox, &password)?;
                info!("Username {}", last_login.selected_profile.name);
                info!("To connect with another user, use signout command.");
                let refreshed_login = refresh_login(server, &last_login, &password).await?;
                info.last_login = Some(refreshed_login.seal(&password)?);
                write_info_dir(&state_dir, &info)?;
                refreshed_login
            }
        }
    };
    info!("Logged in");
    Ok(last_login)
}

async fn signin_user(server: &str, user: &str, password: &str) -> Result<LastLogin> {
    let client_token = new_client_token();
    match authenticate_simple(server, &user, &password, Some(&client_token)).await? {
        AuthenticateResp::Ok(auth) => {
            let profile = match auth.selected_profile {
                None => return Err(Error::ExpectedProfile),
                Some(p) => p,
            };
            Ok(LastLogin {
                access_token: auth.access_token,
                client_token: auth.client_token,
                selected_profile: profile,
            })
        }
        AuthenticateResp::Err(e) => Err(e.into()),
    }
}

async fn refresh_login(server: &str, last_login: &LastLogin, password: &str) -> Result<LastLogin> {
    match refresh_simple(server, &last_login.access_token, &last_login.client_token).await? {
        RefreshResp::Ok(refresh) => {
            let profile = match refresh.selected_profile {
                None => return Err(Error::ExpectedProfile),
                Some(p) => p,
            };
            Ok(LastLogin {
                access_token: refresh.access_token,
                client_token: refresh.client_token,
                selected_profile: profile,
            })
        }
        RefreshResp::Err(err) => {
            info!("Failed to refresh token: {}", err);
            let user: String = ask_username();
            Ok(signin_user(server, &user, password).await?)
        }
    }
}
