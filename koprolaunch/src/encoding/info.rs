use pwbox::{pure::PureCrypto, ErasedPwBox, Eraser, Suite};
use rand::thread_rng;
use semver::Version;
use serde::{Deserialize, Serialize};
use std::path::{Path, PathBuf};
use thiserror::Error;
use uuid::Uuid;
use yggdrasil::{
    refresh::UserProfile,
    token::{AccessToken, ClientToken},
};

pub const INFO_DEFAULT_NAME: &str = ".koprolaunch";

#[derive(Error, Debug)]
pub enum Error {
    #[error("Failed to load from file: {0}")]
    FileError(#[from] std::io::Error),
    #[error("Failed to decode: {0}")]
    DecodingError(#[from] toml::de::Error),
    #[error("Failed to encode: {0}")]
    EncodingError(#[from] toml::ser::Error),
    #[error("State directory is file: {0}")]
    NotDir(PathBuf),
    #[error("Failed to encode encrypted value: {0}")]
    EncryptEncodeError(String),
    #[error("Failed to decode encrypted value: {0}")]
    EncryptDecodeError(#[from] std::str::Utf8Error),
    #[error("Failed to encrypt: {0}")]
    SealError(#[from] anyhow::Error),
    #[error("Failed to decrypt: {0}")]
    UnSealError(String),
}

/// Alias for a `Result` with the error type `self::Error`.
pub type Result<T> = std::result::Result<T, Error>;

#[derive(Deserialize, Serialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
pub struct LauncherInfo {
    #[serde(with = "crate::encoding::version")]
    pub client: Version,
    pub last_login: Option<ErasedPwBox>,
}

impl LauncherInfo {
    pub fn new(v: &Version) -> Self {
        LauncherInfo {
            client: v.clone(),
            last_login: None,
        }
    }
}

#[derive(Deserialize, Serialize, Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Clone)]
#[serde(rename_all = "camelCase")]
pub struct LastLogin {
    pub access_token: AccessToken,
    pub client_token: ClientToken,
    pub selected_profile: UserProfile,
}

impl LastLogin {
    pub fn seal(&self, password: &str) -> Result<ErasedPwBox> {
        let data = toml::to_string(self)?;
        let pwbox = PureCrypto::build_box(&mut thread_rng()).seal(password, data)?;

        let mut eraser = Eraser::new();
        eraser.add_suite::<PureCrypto>();
        Ok(eraser
            .erase(&pwbox)
            .map_err(|e| Error::EncryptEncodeError(e.to_string()))?)
    }

    pub fn unseal(pwbox: &ErasedPwBox, password: &str) -> Result<LastLogin> {
        let mut eraser = Eraser::new();
        eraser.add_suite::<PureCrypto>();
        let data = eraser
            .restore(&pwbox)
            .map_err(|e| Error::UnSealError(e.to_string()))?
            .open(password)
            .map_err(|e| Error::UnSealError(e.to_string()))?;
        let data_str = std::str::from_utf8(&data)?;
        Ok(toml::from_str(data_str)?)
    }

    pub fn offline(username: &str) -> LastLogin {
        LastLogin {
            access_token: Uuid::new_v4().to_string(),
            client_token: Uuid::new_v4().to_string(),
            selected_profile: UserProfile::offline(username),
        }
    }
}

pub fn read_info<P: AsRef<Path>>(file: P) -> Result<LauncherInfo> {
    Ok(toml::from_slice(&std::fs::read(file)?)?)
}

pub fn write_info<P: AsRef<Path>>(file: P, info: &LauncherInfo) -> Result<()> {
    std::fs::write(file, toml::to_vec(info)?)?;
    Ok(())
}

pub fn info_from_dir<P: AsRef<Path>>(dir: P) -> Result<Option<LauncherInfo>> {
    let dref = dir.as_ref();
    if dref.exists() {
        if dref.is_dir() {
            let mut file_path = dref.to_path_buf();
            file_path.push(INFO_DEFAULT_NAME);
            if file_path.exists() {
                Ok(Some(read_info(file_path)?))
            } else {
                Ok(None)
            }
        } else {
            Err(Error::NotDir(dref.to_path_buf()))
        }
    } else {
        Ok(None)
    }
}

pub fn write_info_dir<P: AsRef<Path>>(dir: P, info: &LauncherInfo) -> Result<()> {
    if dir.as_ref().is_dir() {
        let mut file_path = dir.as_ref().to_path_buf();
        file_path.push(INFO_DEFAULT_NAME);
        write_info(file_path, info)
    } else {
        Err(Error::NotDir(dir.as_ref().to_path_buf()))
    }
}

pub fn client_current_version<P: AsRef<Path>>(dir: P) -> Result<Option<Version>> {
    let info = info_from_dir(dir)?;
    Ok(info.map(|i| i.client))
}
