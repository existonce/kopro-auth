use semver::Version;
use serde::{de, Deserialize, Deserializer, Serialize, Serializer};

pub fn serialize<S>(v: &Version, s: S) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    Serialize::serialize(&v.to_string(), s)
}

pub fn deserialize<'de, D>(d: D) -> Result<Version, D::Error>
where
    D: Deserializer<'de>,
{
    let str: String = Deserialize::deserialize(d)?;
    Version::parse(&str).map_err(de::Error::custom)
}
