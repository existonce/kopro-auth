use super::error::*;
use indicatif::{ProgressBar, ProgressStyle};
use std::io::{Seek, SeekFrom, Write};
use yggdrasil_client::endpoints::MetaPayload;
use log::*;

pub async fn download_client(meta: &MetaPayload) -> Result<std::fs::File> {
    let url = match meta.client_link() {
        None => return Err(Error::NoClientLink),
        Some(v) => v,
    };
    let client = reqwest::Client::new();
    let mut res = client.get(url).send().await?;
    let size = res.content_length().unwrap();
    let mut file = tempfile::tempfile()?;
    let mut downloaded_counter = 0;
    let pb = ProgressBar::new(size);
    pb.set_style(ProgressStyle::default_bar()
        .template("{spinner:.green} [{elapsed_precise}] [{wide_bar:.cyan/blue}] {bytes}/{total_bytes} ({eta})")
        .progress_chars("#>-"));
    while let Some(chunk) = res.chunk().await? {
        downloaded_counter += chunk.len();
        pb.set_position(downloaded_counter as u64);
        file.write_all(&chunk)?;
    }
    pb.finish_with_message("downloaded");
    Ok(file)
}

pub fn unzip_client(zip_file: &mut std::fs::File) -> Result<tempfile::TempDir> {
    zip_file.seek(SeekFrom::Start(0))?;
    info!("Unpacking client");
    let mut zip = zip::ZipArchive::new(zip_file)?;
    let client_dir = tempfile::tempdir()?;
    zip.extract(&client_dir)?;
    Ok(client_dir)
}
