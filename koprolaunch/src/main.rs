use clap::Parser;
use koprolaunch::{
    ask_username, check_launcher_update, find_state_dir, launch_client, signout_client,
    update_client, LaunchArgs,
};
use log::*;
use semver::Version;
use std::{error::Error, path::PathBuf, process::Command};
use yggdrasil_client::endpoints::*;

include!(concat!(env!("OUT_DIR"), "/built.rs"));

#[derive(Parser, Debug)]
#[clap(version, about, author = "schoolgirl <existonce@protonmail.com>")]
struct Opts {
    #[clap(
        short,
        long,
    )]
    /// Where to place client data. Default is USER_HOME/.koprokubach for Linux and ./.koprokubach for Windows
    data: Option<PathBuf>,
    #[clap(
        long,
        default_value = "https://auth.kubach.day",
    )]
    /// URL to authentification server. You should specify the option only when there are some DNS issues with default one.
    auth_server: String,
    #[clap(subcommand)]
    subcmd: Option<SubCommand>,
}

#[derive(Parser, Debug)]
enum SubCommand {
    /// Register new user
    Register(RegisterCmd),
    /// Check for updates and download new client
    Update(UpdateCmd),
    /// Launch game
    Launch(LaunchCmd),
    /// Drop saved authentification
    Signout(SignoutCmd),
    /// Change password for user
    ChangePassword(ChangePasswordCmd),
}

#[derive(Parser, Debug)]
struct RegisterCmd {
    #[clap(short, long)]
    user: Option<String>,
    #[clap(short, long)]
    password: Option<String>,
}

#[derive(Parser, Debug)]
struct UpdateCmd {
    #[clap(long)]
    /// Don't download update, only check for it
    dry: bool,
}

#[derive(Parser, Debug)]
struct LaunchCmd {
    #[clap(long)]
    /// Launch game without logging to server
    offline: bool,
    #[clap(long)]
    /// Launch game without checking update
    no_update: bool,
    #[clap(short, long)]
    /// Which username to use on login
    user: Option<String>,
    #[clap(
        short,
        long,
    )]
    /// Which password to use on login. Not recommended as it stored in CLI history in plain text.
    password: Option<String>,
    #[clap(long, default_value = "512M")]
    /// Minimum memory for Java
    min_memory: String,
    #[clap(long, default_value = "4096M")]
    /// Maximum memory for Java
    max_memory: String,
    #[clap(long)]
    /// Additional arguments for Java process
    additional_args: Option<String>,
    #[clap(
        long
    )]
    /// Path to java executable, by default launcher uses PATH to discover it.
    java_path: Option<String>,
    #[clap(long)]
    /// Launch nix shell before start
    nix: bool,
}

impl LaunchCmd {
    pub fn to_launch_args(&self) -> LaunchArgs {
        LaunchArgs {
            min_memory: self.min_memory.clone(),
            max_memory: self.max_memory.clone(),
            additional_args: self.additional_args.as_deref().unwrap_or("").to_string(),
            java_path: self.java_path.clone(),
            use_nix: self.nix,
        }
    }
}
impl Default for LaunchCmd {
    fn default() -> Self {
        LaunchCmd {
            offline: false,
            no_update: false,
            user: None,
            password: None,
            min_memory: "512M".to_owned(),
            max_memory: "4096M".to_owned(),
            additional_args: Some("".to_owned()),
            java_path: None,
            nix: false,
        }
    }
}

#[derive(Parser, Debug)]
struct SignoutCmd {
    #[clap(long)]
    /// Don't request server, only forget local token
    offline: bool,
}

#[derive(Parser, Debug)]
struct ChangePasswordCmd {
    #[clap(short, long)]
    /// Which username to use on login
    user: Option<String>,
    #[clap(
        short,
        long
    )]
    /// Which password to use on login. Not recommended as it stored in CLI history in plain text.
    password: Option<String>,
    #[clap(
        short,
        long
    )]
    /// Which password to change to. Not recommended as it stored in CLI history in plain text.
    new_password: Option<String>,
}

#[tokio::main]
async fn main() -> () {
    env_logger::init_from_env(
        env_logger::Env::default().filter_or(env_logger::DEFAULT_FILTER_ENV, "info"),
    );
    let res = launcher().await;
    if let Err(e) = res {
        error!("{}", e)
    }
    if cfg!(target_os = "windows") {
        let _ = Command::new("cmd.exe").arg("/c").arg("pause").status();
    }
}

async fn launcher() -> Result<(), Box<dyn Error>> {
    let opts: Opts = Opts::parse();

    match opts.subcmd {
        None => launch(opts.data, &opts.auth_server, &LaunchCmd::default()).await?,
        Some(SubCommand::Register(cmd)) => register_cmd(&opts.auth_server, &cmd).await?,
        Some(SubCommand::Update(cmd)) => update(opts.data, &opts.auth_server, &cmd).await?,
        Some(SubCommand::Launch(cmd)) => launch(opts.data, &opts.auth_server, &cmd).await?,
        Some(SubCommand::Signout(cmd)) => {
            signout(opts.data, &opts.auth_server, cmd.offline).await?
        }
        Some(SubCommand::ChangePassword(cmd)) => {
            change_password_cmd(&opts.auth_server, &cmd).await?
        }
    }
    Ok(())
}

async fn register_cmd(auth_server: &str, cmd: &RegisterCmd) -> Result<(), Box<dyn Error>> {
    let user: String = match &cmd.user {
        None => ask_username(),
        Some(user) => user.into(),
    };
    let password: String = match &cmd.password {
        None => rpassword::read_password_from_tty(Some("Password: "))?,
        Some(pass) => pass.into(),
    };
    let second_password: String = match &cmd.password {
        None => rpassword::read_password_from_tty(Some("Repeat password: "))?,
        Some(pass) => pass.into(),
    };
    if password != second_password {
        return Err(koprolaunch::error::Error::PasswordsAreNotMatched.into());
    }
    let res: RegisterResp = register(auth_server, &user, &password).await?;
    match res {
        RegisterResp::Ok(_) => {
            info!("Registered!");
            Ok(())
        }
        RegisterResp::Err(e) => Err(e.into()),
    }
}

async fn update(
    data: Option<PathBuf>,
    auth_server: &str,
    cmd: &UpdateCmd,
) -> Result<(), Box<dyn Error>> {
    let meta = server_meta(auth_server).await?;
    let state_dir = find_state_dir(data)?;
    let own_version = Version::parse(PKG_VERSION).unwrap();
    info!(
        "Launcher update: {}",
        check_launcher_update(&meta, &own_version)
    );
    update_client(&state_dir, &meta, cmd.dry).await?;
    Ok(())
}

async fn launch(
    data: Option<PathBuf>,
    auth_server: &str,
    cmd: &LaunchCmd,
) -> Result<(), Box<dyn Error>> {
    if cmd.offline {
        info!("Starting in offline mode. You will not able to log to any server.");
    }
    let meta = if cmd.offline {
        MetaPayload::default()
    } else {
        server_meta(auth_server).await?
    };
    let state_dir = find_state_dir(data)?;
    if !cmd.no_update && !cmd.offline {
        let own_version = Version::parse(PKG_VERSION).unwrap();
        info!(
            "Launcher update: {}",
            check_launcher_update(&meta, &own_version)
        );
        update_client(&state_dir, &meta, false).await?;
    }
    let mut child = launch_client(
        auth_server,
        &state_dir,
        &meta,
        cmd.user.as_deref(),
        cmd.password.as_deref(),
        &cmd.to_launch_args(),
        cmd.offline,
    )
    .await?;
    child.wait()?;
    Ok(())
}

async fn signout(
    data: Option<PathBuf>,
    auth_server: &str,
    offline: bool,
) -> Result<(), Box<dyn Error>> {
    let state_dir = find_state_dir(data)?;
    signout_client(auth_server, &state_dir, offline).await?;
    Ok(())
}

async fn change_password_cmd(
    auth_server: &str,
    cmd: &ChangePasswordCmd,
) -> Result<(), Box<dyn Error>> {
    let user: String = match &cmd.user {
        None => ask_username(),
        Some(user) => user.into(),
    };
    let password: String = match &cmd.password {
        None => rpassword::read_password_from_tty(Some("Password: "))?,
        Some(pass) => pass.into(),
    };
    let new_password: String = match &cmd.new_password {
        None => rpassword::read_password_from_tty(Some("New password: "))?,
        Some(pass) => pass.into(),
    };
    let second_password: String = match &cmd.new_password {
        None => rpassword::read_password_from_tty(Some("Repeat new password: "))?,
        Some(pass) => pass.into(),
    };
    if new_password != second_password {
        return Err(koprolaunch::error::Error::PasswordsAreNotMatched.into());
    }
    change_password(auth_server, &user, &password, &new_password).await?;
    info!("Password changed");
    Ok(())
}
