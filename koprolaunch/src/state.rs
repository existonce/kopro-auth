use super::error::Error;
use crate::info::INFO_DEFAULT_NAME;
use log::*;
use std::path::{Path, PathBuf};

pub const DEFAULT_STATE_DIR_NAME: &str = ".koprokubach";
pub const LOCATION_FILE_NAME: &str = ".koprolocation";

pub fn find_state_dir(data: Option<PathBuf>) -> Result<PathBuf, Error> {
    let state_dir = data.map_or_else(
        || {
            Ok(read_cached_location()
                .unwrap_or_else(|| find_existing_dir().unwrap_or_else(default_state_dir)))
        },
        |p| {
            if p.exists() {
                if p.is_dir() {
                    let mut info_path = p.clone();
                    info_path.push(INFO_DEFAULT_NAME);
                    if info_path.is_file() {
                        info!("Found existing state dir at {}", p.display());
                        Ok(p)
                    } else {
                        error!(
                            "Path {} is existing dir without {INFO_DEFAULT_NAME} file! Aborting", p.display()
                        );
                        Err(Error::InvalidStateDir(format!(
                            "Path {} is existing dir without {INFO_DEFAULT_NAME} file!", p.display()
                        )))
                    }
                } else {
                    error!("Path {} is a existing file!", p.display());
                    Err(Error::InvalidStateDir(format!(
                        "Path {} is a existing file!", p.display()
                    )))
                }
            } else {
                Ok(p)
            }
        },
    )?;
    write_cached_location(&state_dir);
    Ok(state_dir)
}

fn state_dir_canditates() -> Vec<PathBuf> {
    let mut home = dirs::home_dir().unwrap();
    home.push(DEFAULT_STATE_DIR_NAME);
    if cfg!(target_os = "windows") || cfg!(target_os = "macos") {
        let mut local_path = std::env::current_exe().unwrap();
        local_path.pop();
        local_path.push(DEFAULT_STATE_DIR_NAME);
        vec![local_path, home]
    } else {
        vec![home]
    }
}

fn default_state_dir() -> PathBuf {
    state_dir_canditates().first().unwrap().clone()
}

fn find_existing_dir() -> Option<PathBuf> {
    state_dir_canditates()
        .iter()
        .find(|&p| {
            let existed = p.exists();
            if existed {
                info!("Found existing state dir at {:?}", p);
            }
            existed
        })
        .cloned()
}

fn write_cached_location(state: &Path) {
    let path = cached_location_path();
    info!("Writing state dir to {:?}", path);
    std::fs::write(path, state.to_str().unwrap()).unwrap();
}

fn read_cached_location() -> Option<PathBuf> {
    let path = cached_location_path();
    if path.exists() {
        let content = std::fs::read_to_string(&path).unwrap().into();
        info!("Detected state dir from {:?}: {:?}", path, content);
        Some(content)
    } else {
        None
    }
}

fn cached_location_path() -> PathBuf {
    if cfg!(target_os = "windows") || cfg!(target_os = "macos") {
        let mut path = std::env::current_exe().unwrap();
        path.pop();
        path.push(LOCATION_FILE_NAME);
        path
    } else {
        let mut path = dirs::home_dir().unwrap();
        path.push(LOCATION_FILE_NAME);
        path
    }
}
