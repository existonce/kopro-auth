use super::backup::*;
use super::download::*;
use super::error::*;
use crate::encoding::info::{client_current_version, write_info_dir, LauncherInfo};
use fs_extra::dir::CopyOptions;
use semver::Version;
use std::fmt::Display;
use std::path::Path;
use yggdrasil_client::endpoints::MetaPayload;
use log::*;

pub const ARCHIVE_ROOT_DIR: &str = "koprokubach-client";

/// Result of compare with optional download link for result
#[derive(Debug)]
pub enum CheckRes {
    NoUpdates,
    MinorUpdate(Version, Option<String>),
    MajorUpdate(Version, Option<String>),
}

impl Display for CheckRes {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            CheckRes::NoUpdates => write!(f, "Up to date"),
            CheckRes::MinorUpdate(v, murl) => match murl {
                Some(url) => write!(
                    f,
                    "There is new minor version {}, download it at {}",
                    v, url
                ),
                None => write!(f, "There is new minor version {}", v),
            },
            CheckRes::MajorUpdate(v, murl) => match murl {
                Some(url) => write!(
                    f,
                    "There is new major version {}, download it at {}",
                    v, url
                ),
                None => write!(f, "There is new major version {}", v),
            },
        }
    }
}

fn check_update<'a, F>(remote: Option<&Version>, own: &Version, get_link: F) -> CheckRes
where
    F: FnOnce() -> Option<&'a str>,
{
    if let Some(v) = remote {
        if v.major > own.major {
            let link = get_link().map(|v| v.to_string());
            CheckRes::MajorUpdate(v.clone(), link)
        } else if v.minor > own.minor {
            let link = get_link().map(|v| v.to_string());
            CheckRes::MinorUpdate(v.clone(), link)
        } else {
            CheckRes::NoUpdates
        }
    } else {
        CheckRes::NoUpdates
    }
}

pub fn check_launcher_update(meta: &MetaPayload, own: &Version) -> CheckRes {
    check_update(meta.meta.launcher_version.as_ref(), own, || {
        meta.launcher_link()
    })
}

pub fn check_client_update(meta: &MetaPayload, own: &Version) -> CheckRes {
    check_update(meta.meta.client_version.as_ref(), own, || {
        meta.client_link()
    })
}

pub async fn update_client<P: AsRef<Path> + Clone>(
    state_dir: P,
    meta: &MetaPayload,
    dry: bool,
) -> Result<()> {
    let version = match &meta.meta.client_version {
        None => return Err(Error::NoClientVersion),
        Some(v) => v,
    };
    let mcur_ver = client_current_version(&state_dir)?;
    match mcur_ver {
        Some(cur_ver) if cur_ver == *version => {
            info!("Client is up to date with version {}", version);
            Ok(())
        }
        _ => {
            info!("Updating to version {}", version);
            let mut zip_file = download_client(meta).await?;
            let tmp_client = unzip_client(&mut zip_file)?;
            if dry {
                info!(
                    "Dry run, unpacked client is located at {:?}",
                    tmp_client.into_path()
                );
            } else {
                backup_client(state_dir.clone())?;
                let mut sub_client = tmp_client.path().to_path_buf();
                sub_client.push(ARCHIVE_ROOT_DIR);
                match mcur_ver {
                    None => client_full_copy(&state_dir, &sub_client)?,
                    Some(cur_ver) if version.major > cur_ver.major => {
                        client_major_copy(&state_dir, &sub_client)?
                    }
                    Some(_) => client_minor_copy(&state_dir, &sub_client)?,
                }
                write_info_dir(&state_dir, &LauncherInfo::new(version))?
            }
            Ok(())
        }
    }
}

pub fn client_full_copy<P1: AsRef<Path>, P2: AsRef<Path>>(
    state_dir: P1,
    temp_client: P2,
) -> Result<()> {
    info!(
        "Performing full copy of client from {:?} to {:?}",
        temp_client.as_ref(),
        state_dir.as_ref()
    );
    if state_dir.as_ref().exists() {
        std::fs::remove_dir_all(&state_dir)?;
    }
    std::fs::create_dir_all(&state_dir)?;
    let mut options = fs_extra::dir::DirOptions::new();
    options.depth = 1;
    let mut dir_content = fs_extra::dir::get_dir_content2(&temp_client, &options)?;
    let mut from = dir_content.directories;
    from.append(&mut dir_content.files);
    fs_extra::copy_items(&from[1..], &state_dir, &CopyOptions::new())?;
    Ok(())
}

/// Important sub directories and files of client that are able to be updated
fn client_parts() -> Vec<&'static str> {
    vec![
        "asm",
        "assets",
        "config",
        "libraries",
        "scripts",
        "servers.dat",
        "mods",
        "versions",
        "start.sh",
        "start.bat",
    ]
}

pub fn client_major_copy<P1: AsRef<Path>, P2: AsRef<Path>>(
    state_dir: P1,
    temp_client: P2,
) -> Result<()> {
    info!(
        "Performing major copy of client from {:?} to {:?}",
        temp_client.as_ref(),
        state_dir.as_ref()
    );
    let to_replace = client_parts();
    remove_names(&state_dir, &to_replace)?;
    copy_names(&temp_client, &state_dir, &to_replace, &CopyOptions::new())
}

pub fn client_minor_copy<P1: AsRef<Path>, P2: AsRef<Path>>(
    state_dir: P1,
    temp_client: P2,
) -> Result<()> {
    info!(
        "Performing minor copy of client from {:?} to {:?}",
        temp_client.as_ref(),
        state_dir.as_ref()
    );
    let to_replace = client_parts();
    let mut opts = CopyOptions::new();
    opts.overwrite = true;
    copy_names(&temp_client, &state_dir, &to_replace, &opts)
}

fn remove_names<P: AsRef<Path>>(dir: P, names: &[&str]) -> Result<()> {
    let mut to_remove = vec![];
    for name in names.iter() {
        let mut path = dir.as_ref().to_path_buf();
        path.push(name);
        if path.exists() {
            to_remove.push(path);
        }
    }
    fs_extra::remove_items(&to_remove)?;
    Ok(())
}

fn copy_names<P1: AsRef<Path>, P2: AsRef<Path>>(
    dir_from: P1,
    dir_to: P2,
    names: &[&str],
    opts: &CopyOptions,
) -> Result<()> {
    let mut to_copy = vec![];
    for name in names.iter() {
        let mut path = dir_from.as_ref().to_path_buf();
        path.push(name);
        if path.exists() {
            to_copy.push(path);
        }
    }
    fs_extra::copy_items(&to_copy, &dir_to, opts)?;
    Ok(())
}
