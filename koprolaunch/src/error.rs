use super::encoding;
use thiserror::Error;

#[derive(Error, Debug)]
pub enum Error {
    #[error("Reqwesting server error: {0}")]
    ReqwestErr(#[from] reqwest::Error),
    #[error("Launcher information file error: {0}")]
    InfoErr(#[from] encoding::info::Error),
    #[error("There is no client version found in server response")]
    NoClientVersion,
    #[error("There is no client link found in server response")]
    NoClientLink,
    #[error("Failed IO operation: {0}")]
    FileErr(#[from] std::io::Error),
    #[error("Failed zip operation: {0}")]
    ZipErr(#[from] zip::result::ZipError),
    #[error("Failed backup: {0}")]
    BackupErr(#[from] fs_extra::error::Error),
    #[error("Cannot find launcher meta information in {0}")]
    ExpectedVersionInfo(String),
    #[error("Server doesn't provide profile for login")]
    ExpectedProfile,
    #[error("Authentification server error {0}")]
    YggdrasilError(#[from] yggdrasil::error::Error),
    #[error("Passwords are not matched, try again")]
    PasswordsAreNotMatched,
    #[error("State directory is invalid: {0}")]
    InvalidStateDir(String),
}

/// Alias for a `Result` with the error type `self::Error`.
pub type Result<T> = std::result::Result<T, Error>;
